﻿Generate a NovoKey license or validate a NovoKey license.
Usage: novolic [-help] [-gen key] [-validate licensefile]
Examples: 
	novolic -gen xxxxx-xxxxx-xxxxx-xxxxx-xxxxx
	novolic -validate license.lic

There is a hidden feature for developer to see the result in clear text. May remove this for production version.
add -cleattext to the parameter

The output string is encoded with 3DES algorithm. http://en.wikipedia.org/wiki/Triple_DES
The key used to encrypt the output is the first 8 bytes of the key string except "-"
