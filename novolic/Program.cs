﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delta.NovoKey;

namespace novolic
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Command line parsing
            var commandLine = new Arguments(args);

            if (commandLine["help"] != null)
            {
                var sb = new StringBuilder();
                sb.AppendLine("Generate a NovoKey license or validate a NovoKey license.");
                sb.AppendLine("novolic [-help] [-gen key] [-validate licensefile]");
            }

            if (commandLine["cleartext"] != null)
            {
                if (commandLine["gen"] != null)
                {
                    string key = commandLine["gen"].Trim();
                    try
                    {
                        Validator.GenerateNewLicense(key);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
                else if (commandLine["validate"] != null)
                {
                    var licenseFile = commandLine["validate"].Trim();
                    License lic = License.ReadLicense(licenseFile);
                    if (lic != null)
                    {
                        License.Status state = lic.GetStatus();

                        switch (state)
                        {
                            case License.Status.Invalid:
                                Console.WriteLine("License key is invalid. Please try again.");
                                break;
                            case License.Status.TrialExpired:
                                Console.WriteLine("License key is expired.");
                                break;
                            case License.Status.Blacklisted:
                                Console.WriteLine(
                                    "Your license key has been blocked. Please contact support service.");
                                break;
                            case License.Status.IsValid:
                                Console.WriteLine("License key is valid.");
                                break;
                            case License.Status.NotFound:
                                Console.WriteLine("License file could not be found. Please contact support service.");
                                break;
                            case License.Status.IsValidTrialVersion:
                                Console.WriteLine("This ia a trial version. You have " + lic.GetTrialDaysLeft() +
                                                  " days left");

                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("License file could not be found. Please contact support service.");
                    }
                }
            }
            else
            {
                if (commandLine["gen"] != null)
                {
                    string key = commandLine["gen"].Trim();

                    string str = new string(key.Replace("-", string.Empty).Take(8).ToArray());
                    byte[] bytes = new byte[str.Length * sizeof(char)];
                    Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

                    try
                    {
                        Validator.GenerateNewLicense(key);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
                else if (commandLine["validate"] != null)
                {
                    var licenseFile = commandLine["validate"].Trim();
                    License lic = License.ReadLicense(licenseFile);
                    if (lic != null)
                    {

                        string key = lic.ProductKey;

                        string str = new string(key.Replace("-", string.Empty).Take(8).ToArray());
                        byte[] bytes = new byte[str.Length * sizeof(char)];
                        Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

                        License.Status state = lic.GetStatus();
                        
                        switch (state)
                        {
                            case License.Status.Invalid:
                                Console.WriteLine(DES.Encrypt("Invalid", bytes));
                                break;
                            case License.Status.TrialExpired:
                                Console.WriteLine(DES.Encrypt("TrialExpired", bytes));
                                break;
                            case License.Status.Blacklisted:
                                Console.WriteLine(DES.Encrypt("Blacklisted", bytes));
                                break;
                            case License.Status.IsValid:
                                Console.WriteLine(DES.Encrypt("IsValid", bytes));
                                break;
                            case License.Status.NotFound:
                                Console.WriteLine(DES.Encrypt("NotFound", bytes));
                                break;
                            case License.Status.IsValidTrialVersion:
                                Console.WriteLine( DES.Encrypt("IsValidTrialVersion: " + lic.GetTrialDaysLeft() +
                                                  " days", bytes));

                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("License file could not be found. Please contact support service.");
                    }
                }
            }
        }
    }
}
