﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delta.NovoKey;
using Delta.NovoKey.Generator;
using NUnit.Framework;

namespace NovoKeyTests
{
    [TestFixture]
    public class LicenseTests
    {
        IProductKeyService keyService;
        private int keyVersion;
        private static string DefaultLicenseFilePath = "C:\\delta\\AppData\\License";
        [TestFixtureSetUp]
        public void Init()
        {
            keyVersion = 1;
            keyService = new ProductKeyService();
            var fi = new FileInfo("NewLicense.lic");
            if(fi.Exists)
                fi.Delete();
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            var fi = new FileInfo("NewLicense.lic");
            if (fi.Exists)
                fi.Delete();

            string _licenseGenerationRecordFile =
            DefaultLicenseFilePath + "\\Delta Electronics\\nvkdat.dll";

            fi = new FileInfo(_licenseGenerationRecordFile);
            if (fi.Exists)
                fi.Delete();
        }
        
        
        //[Test]
        //[ExpectedException(typeof(TrialKeyExpiredException))]
        //public void TestExpiredLicenseFile()
        //{
        //    String licenseFileName = "ExpiredLicense.lic";
        //    var licenseInfo = new KeyLicenseInfo();
        //    licenseInfo.ProductID = 1;
        //    licenseInfo.LicenseType = License.LicenseType.Trial;
        //    licenseInfo.TrialDays = -1;
        //    licenseInfo.NumOfCopies = 1;
        //    string key = keyService.GenerateProductKey(keyVersion, licenseInfo, false);

        //    var fi = new FileInfo(licenseFileName);
        //    if (fi.Exists)
        //        fi.Delete();

        //    Validator.GenerateNewLicense(licenseFileName, key, "Eddie Chen", "eddie.chen@delta.com.tw", false);
        //    fi = new FileInfo(licenseFileName);
        //    if (fi.Exists)
        //    {
        //        License lic = License.ReadLicense(licenseFileName);
        //        var status = lic.GetStatus();
        //        Assert.AreEqual(License.Status.TrialExpired, status);
        //    }
        //    else
        //    {
        //        Assert.Fail();
        //    }

        //    Assert.False(License.ValidateLicense("ExpiredLicense.lic","unit test", "test"));
        //}

        //[Test]
        //public void TestNonExistingLicenseFile()
        //{
        //    var lic = License.ReadLicense("nonExistentLicense");
        //    Assert.AreEqual(License.Status.NotFound, lic.LicenseStatus);
        //    Assert.AreEqual(License.LicenseType.Invalid, lic.Type);
        //}

        //[Test]
        //public void TestIsValidTrialVersion()
        //{
        //    var licenseInfo = new KeyLicenseInfo();
        //    licenseInfo.ProductID = 1;
        //    licenseInfo.LicenseType = License.LicenseType.Trial;
        //    licenseInfo.TrialDays = 30;
        //    licenseInfo.NumOfCopies = 1;
        //    string key = keyService.GenerateProductKey(keyVersion, licenseInfo, false);

        //    Validator.GenerateNewLicense("TrialLicense.lic", key, "Eddie Chen", "eddie.chen@delta.com.tw", false);

        //    const string licenseFileName = "TrialLicense.lic";
        //    var fi = new FileInfo(licenseFileName);
        //    if (fi.Exists)
        //    {
        //        License lic = License.ReadLicense(licenseFileName);
        //        var status = lic.GetStatus();
        //        Assert.AreEqual(License.Status.IsValidTrialVersion, lic.LicenseStatus);
        //        Assert.AreEqual(License.LicenseType.Trial, lic.Type);
        //    }
        //    else
        //    {
        //        Assert.Fail();
        //    }

        //    Assert.True(License.ValidateLicense("TrialLicense.lic", "unit test", "test"));

        //}

        //[Test]
        //public void TestGeneratingNewLicense()
        //{
        //    var licenseInfo = new KeyLicenseInfo { ProductID = 1, LicenseType = License.LicenseType.Single, TrialDays = 0, NumOfCopies = 1 };
        //    string key = keyService.GenerateProductKey(keyVersion, licenseInfo, false);

        //    string key = "PIQXJ-E560Q-U240C-6I10R-1Y1MA-2200O";

        //    Validator.GenerateNewLicense("NewLicense.lic", key, "Eddie Chen", "eddie.chen@delta.com.tw", false);

        //    var fi = new FileInfo("NewLicense.lic");
        //    License lic = null;
        //    if (fi.Exists)
        //        lic = License.ReadLicense("NewLicense.lic");

        //    Assert.True(fi.Exists);
        //    Assert.NotNull(lic);
        //    Assert.True(License.ValidateLicense("NewLicense.lic", "unit test", "test"));
        //    Assert.AreEqual(DateTime.Now.ToShortDateString(), lic.ActivationDate.ToShortDateString());
        //    Assert.AreEqual(key, lic.ProductKey);

        //}

    }
}
