﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delta.NovoKey;
using Delta.NovoKey.Generator;
using NUnit.Framework;

namespace NovoKeyTests
{
    [TestFixture]
    public class DevelopmentKeysTests
    {
        IProductKeyService keyService;
        [TestFixtureSetUp]
        public void Init()
        {
            keyService = new ProductKeyService();
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
        }

        [Test]
        public void VerifyDeltaFreeKey()
        {
            Assert.True(Validator.VerifyKey("3V2UP-U560Q-T1Y0C-6I10C-1Y1MA-0W00O",false));
        }

        [Test]
        public void VerifyDeltaTrialKey()
        {
            Assert.True(Validator.VerifyKey("HNM80-O560Q-A200D-12108-1Y1MS-6A007",false));
        }

        [Test]
        public void VerifyDeltaVolKey()
        {
            Assert.True(Validator.VerifyKey("48SDN-W560Q-4220C-6I10I-1Y1MT-1O00R", false));
        }

        [Test]
        public void VerifyDeltaSingleKey()
        {
            Assert.True(Validator.VerifyKey("EY2PT-1560Q-4240C-6I10I-1Y1ML-2200R", false));
        }

        [Test]
        public void VerifyDeltaSubscriptionKey()
        {
            Assert.True(Validator.VerifyKey("YRSUS-2560Q-2260C-6I10U-1Y1MO-2G00G", false));
        }

        [Test]
        public void VerifyNXiDEAFreeKey()
        {
            Assert.True(Validator.VerifyKey("FWMDJ-U560Q-W1Y0C-6I102-1Y1MD-0W007", false));
        }

        [Test]
        public void VerifyNXiDEATrialKey()
        {
            Assert.True(Validator.VerifyKey("DI2D1-B560Q-M200D-1210Z-1Y1MA-6A00E", false));
        }

        [Test]
        public void VerifyNXiDEAVolKey()
        {
            Assert.True(Validator.VerifyKey("CG0EK-S560Q-4220C-6I10I-1Y1ML-1O00R", false));
        }

        [Test]
        public void VerifyNXiDEASingleKey()
        {
            Assert.True(Validator.VerifyKey("LPDHO-A560Q-X240C-6I10A-1Y1MB-2200B", false));
        }

        [Test]
        public void VerifyNXiDEASubscriptionKey()
        {
            Assert.True(Validator.VerifyKey("2ERL4-G560Q-Q260C-6I10G-1Y1MS-2G00L", false));
        }
    }
}
