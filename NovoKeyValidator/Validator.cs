﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using Delta.NovoKey.Exceptions;
using RestSharp;
using System.Runtime.InteropServices;

//using NLog;

namespace Delta.NovoKey
{
    /// <summary>
    /// Provides key validation service
    /// </summary>
    public class Validator
    {
        //private const int ProductID = 1;
        //const string LicenseServerIp = "service.novosync.com";
       // const string LicenseServerIp = "172.16.5.65";
        //private const string LicenseServerBaseUrl = "https://" + LicenseServerIp + "/key";
        //private const string LicenseServerPingUrl = "https://" + LicenseServerIp + "/ping";
        private static string DefaultLicenseFile = "";
        private static string DefaultLicenseFilePath = "C:\\delta\\AppData\\License";
           // DefaultLicenseFilePath + "\\Delta Electronics\\license.lic";

        //[DllImport("wininet.dll",CharSet=CharSet.Auto)] 
        //static extern bool InternetGetConnectedState(ref ConnectionState lpdwFlags, int dwReserved);
        //private static ConnectionState connectionState = 0;

        public static bool VerifyKey(string key)
        {
            return VerifyKey(key, WebRequest.DefaultWebProxy);
        }

        public static bool VerifyKey(string key, bool sendToLicenseServer)
        {
            return VerifyKey(key, sendToLicenseServer, WebRequest.DefaultWebProxy);
        }

        public static bool VerifyKey(string key, IWebProxy proxy)
        {
            return VerifyKey(key, true, proxy);
        }

        /// <summary>
        /// Verify the key
        /// </summary>
        /// <param name="key">Product Key String</param>
        /// <param name="sendToLicenseServer">Set true to determine whether the key is blocked by the license server</param>
        /// <returns>bool</returns>
        public static bool VerifyKey(string key, bool sendToLicenseServer, IWebProxy proxy)
        {
            //
            // If they accidentally put spaces at the start/end,
            // take them out for them.
            //
            key = key.Trim();
            //
            // Make sure they haven't put garbage in.
            // If they have, give them no clues that
            // it's the wrong length! 
            //
            if (key.Length != 35)
            {
                return false;
            }

            // connect to license server to check whether the key is blacklisted or not

            //
            // Create the list that will contain the 'Char Arrays'
            //
            var lst = new List<List<string>>();


            // ################################################################
            // #  VERY VERY IMPORTANT!!!!                                     #
            // #                                                              #
            // #  Initialize __SOME__ of the arrays used in generating        #
            // #  a key.                                                      #
            // #  THIS IS YOUR MAIN WEAPON AGAINST HACKERS!!!                 #
            // #  Change the value when releasing a new version of keygen     #
            // ################################################################

            string str1 = "AABCCDEEFGGHIIJKKLMMNOOPQQRSSTUUVWWXYYZ001223445667889";
            string str6 = "LXMNTOPUVQWAK0245689";
            string str15 = "QAZXCDERTGBNMJUIOL1548623";
            string str20 = "OBAMALEADSNEWPOLLSPRESIDENT";
            string str25 = "MBEROFPOLLSTHEGRIO24579";

            IEnumerable<string> arr1 = CConvert.GetArr(str1);
            IEnumerable<string> arr6 = CConvert.GetArr(str6);
            IEnumerable<string> arr15 = CConvert.GetArr(str15);
            IEnumerable<string> arr20 = CConvert.GetArr(str20);
            IEnumerable<string> arr25 = CConvert.GetArr(str25);

            //
            // Add the few initialized arrays into the list,
            // using blanks wherever we've left one out
            //

            lst.Add(new List<string>(arr1));
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());

            lst.Add(new List<string>(arr6));
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());

            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>(arr15));

            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>(arr20));

            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>());
            lst.Add(new List<string>(arr25));

            //
            // Be prepared for more garbage
            //
            int seed;
            try
            {
                seed = (int) CConvert.FromBase36(key.Substring(0, 5));
            }
            catch
            {
                return false;
            }

            //
            // Create the regular expression to test
            // the key against.
            //
            var chk = key.Substring(0, 5);
            var rng = new RandomNumber();

            while (chk.Length != 35)
            {
                int x = chk.Length;
                // Use modulus to determine where "-" chars belong
                if (x%6 == 5)
                {
                    chk += "-";
                }
                else if (x + 1 == 8)
                {
                    chk += "..";
                }
                else if (x + 1 == 10)
                {
                    chk += "..";
                }
                else if (x + 1 == 14)
                {
                    chk += "..";
                }
                else if (x + 1 == 16)
                {
                    chk += "..";
                }
                else if (x + 1 == 19)
                {
                    chk += "..";
                }
                else if (x + 1 == 21)
                {
                    chk += "..";
                }
                else if (x + 1 == 25)
                {
                    chk += "..";
                }
                else if (x + 1 == 27)
                {
                    chk += "..";
                }
                else if (x + 1 == 31)
                {
                    chk += "..";
                }
                else if (x + 1 == 33)
                {
                    chk += "..";
                }
                else
                {
                    // Check to see if the list actually contains anything
                    if (lst[(int) (x - Math.Floor((double) (5 + (x + 1)/6)))].Count == 0)
                    {
                        // Indicate that this character can be anything
                        chk += ".";
                        // Tick the random number generator along
                        // r1.Next(0, 1);
                        rng.GetRandomNumber((uint) seed, 0, 1);
                        //NextInt(0, 1);
                    }
                    else
                    {
                        // Give the required value of this character
                        //int k = r1.Next(0, lst[(int)(x - Math.Floor((double)(5 + (x + 1) / 6)))].Count - 1);
                        int k = rng.GetRandomNumber((uint) seed, 0,
                                                    lst[(int) (x - Math.Floor((double) (5 + (x + 1)/6)))].Count - 1);
                        // int k = NextInt(0, lst[(int)(x - Math.Floor((double)(5 + (x + 1) / 6)))].Count - 1);

                        chk += lst[(int) (x - Math.Floor((double) (5 + (x + 1)/6)))][k];
                    }
                }
            }

            //if (IsRevoked(key))
            //    throw new KeyRevokedException("Your product key is revoked. Please get a new product key!");

            if (sendToLicenseServer)
            {
              //  bool result = false;
               //     result = Regex.IsMatch(key, chk) && License.Validate(key, proxy).Status.Equals("AVAILABLE_KEY");

                //if(result == false)
               //     throw new Exception("Local key validation: " + Regex.IsMatch(key, chk).ToString() + " | Server validation: " + License.Validate(key, proxy).Status);

               // return Regex.IsMatch(key, chk) && License.Validate(key, proxy).ActivationStatus.Equals(true);
                string result = License.Validate(key, false, proxy).Status;
                return Regex.IsMatch(key, chk) && (result == "AVAILABLE_KEY" || result == "SUCCESS");
            }
            else
            {
                return Regex.IsMatch(key, chk);
            }
        }
        
        /// <summary>
        /// Generate a new license key with file name "license.lic" in the same directory
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>bool</returns>
        //public static void GenerateNewLicense(string key, string customerName, string customerEmail)
        //{
        //    GenerateNewLicense(DefaultLicenseFile, key, customerName, customerEmail);
        //}

        public static void GenerateNewLicense(string key, string customerName, string customerEmail)
        {
            GenerateNewLicense(key, customerName, customerEmail, WebRequest.DefaultWebProxy);
        }

        public static void GenerateNewLicense(string key, string customerName, string customerEmail, string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            GenerateNewLicense(key, customerName, customerEmail, proxy);
        }

        /// <summary>
        /// Generate a new license key with file name specified in licenseFile
        /// </summary>
        /// <param name="licenseFile"></param>
        /// <param name="key"></param>
        /// <param name="customerName"></param>
        /// <param name="customerEmail"></param>
        public static void GenerateNewLicense(string key, string customerName, string customerEmail, IWebProxy proxy)
        {
            if (proxy.Credentials == null)
                proxy.Credentials = CredentialCache.DefaultCredentials;

            /*
            if (proxy.Credentials == null)
            {
                string credential = "No credential INFO!";
                Uri uri = new Uri("https://service.novosync.com");
                if (proxy.Credentials != null)
                    credential = " Proxy used: " + proxy.GetProxy(uri).AbsoluteUri + ", credential is not NULL";

                throw new Exception(credential);
            }
            */
            GenerateNewLicense(key, customerName, customerEmail, true, proxy);
        }

        /// <summary>
        /// Generate a new license key with file name specified in licenseFile, with the option to activate with the server or not
        /// </summary>
        /// <param name="licenseFile">Full path of the license file</param>
        /// <param name="key">key string</param>
        /// <param name="customerName"></param>
        /// <param name="customerEmail"></param>
        /// <param name="sendToLicenseServer"></param>
        /// <returns>bool</returns>
        public static void GenerateNewLicense(string key, string customerName, string customerEmail, bool sendToLicenseServer, IWebProxy proxy)
        {
            bool result = false;

            if (sendToLicenseServer)
            {
                result = VerifyKey(key, proxy);
            }
            else
                result = VerifyKey(key, false, proxy);

            KeyLicenseInfo info = License.GetLicenseInfoFromKey(key);
            DefaultLicenseFile = License.GetDefaultLicenseFile(info.ProductID);

            if (result)
            {
                var di = new DirectoryInfo(DefaultLicenseFilePath + "\\Delta Electronics\\");
                if(!di.Exists)
                    di.Create();
                
                if (info.LicenseType == License.LicenseType.Trial )
                {
                    var tlic = new License();
                    var licRecord = tlic.ReadTrialLicenseRecord(info.ProductID);
                    if (licRecord != null)
                    {
                        if (licRecord.CanGenerateTrialLicense)
                        {
                            DeleteLicenseFile(DefaultLicenseFile);
                        }
                    }

                    var lic = new License(info);
                    lic.Activated = false;
                    lic.NumberOfAccounts = 1;
                    lic.NumberOfConcurrentUsers = 1;
                    lic.CustomerName = customerName;
                    lic.CustomerEmail = customerEmail;
                    lic.Save();

                    try
                    {
                        if (sendToLicenseServer)
                        {
                            License.Activate(info.ProductID, proxy);
                        }

                    }
                    catch (NoMoreActivationException)
                    {
                        throw;
                    }
                    catch (TrialKeyExpiredException)
                    {
                        throw;
                    }

                    catch (Exception ex)
                    {
                        throw new AccessViolationException("Cannot save license file. " + ex.Message);
                    }
                }
                else
                {
                    DeleteLicenseFile(DefaultLicenseFile);
                    var lic = new License(info);
                    lic.Activated = false;
                    lic.NumberOfAccounts = 1;
                    lic.NumberOfConcurrentUsers = 1;
                    lic.LicenseStatus = License.Status.IsValid;
                    lic.CustomerName = customerName;
                    lic.CustomerEmail = customerEmail;
                    lic.Save();

                    try
                    {
                        if (sendToLicenseServer)
                            License.Activate(info.ProductID, proxy);
                    }
                    catch (TrialKeyExpiredException)
                    {
                        throw;
                    }

                    catch (NoMoreActivationException)
                    {
                        throw;
                    }

                    catch (Exception ex)
                    {
                        throw new AccessViolationException("Cannot save license file. " + ex.Message);
                    }
                }
            }
            else
            {
                string credential = "No credential info!";
                Uri uri = new Uri("https://service.novosync.com");
                if (proxy.Credentials != null)
                    credential = " Proxy used:" + proxy.GetProxy(uri).AbsoluteUri + ", credential is not null";
                

                throw new InvalidProductKeyException("License generation failed. Invalid key detected." + credential);
            }
            
            //return result;
        }

        private static void DeleteLicenseFile(string licenseFile)
        {
            var fi = new FileInfo(licenseFile);

            if (fi.Exists)
            {
                try
                {
                    fi.Delete();
                }
                catch (Exception ex)
                {
                    throw new AccessViolationException("Cannot delete license file. " + ex.Message);
                }
            }
        }
    }
}