md C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\bin\NovoKey\NovoKey.j4n.* C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\bin\NovoKeyValidator\NovoKeyValidator.j4n.* C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\bin\NovoKey.dll C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\bin\NovoKeyValidator.dll C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\bin\protobuf-net.dll C:\Jenkins\workspace\NovoKey\NovoKeyOutput
copy C:\Jenkins\workspace\NovoKey\JNI\lib\*.* C:\Jenkins\workspace\NovoKey\NovoKeyOutput