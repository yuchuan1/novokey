﻿using System;
using System.Net;
using RestSharp;

namespace Delta.NovoKey
{
    public class PingLicenseServer
    {
        private const string _serverDomainName = "service.novosync.com";

        public static bool Ping()
        {
            return Ping(WebRequest.DefaultWebProxy);
        }

        public static bool Ping(string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            return Ping(proxy);
        }

        public static bool Ping(IWebProxy proxy)
        {
            IRestClient client = new RestClient
            {
                BaseUrl = "http://" + _serverDomainName + "/ping"
            };
            client.Proxy = proxy;
            if (proxy.Credentials == null)
                client.Proxy.Credentials = CredentialCache.DefaultCredentials;

            var request = new RestRequest { Method = Method.GET };

            try
            {
                var response = (RestResponse)client.Execute(request);

                if (response.ResponseStatus != ResponseStatus.Completed)
                    return false;

                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        if(response.Content.Equals("1"))
                            return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
