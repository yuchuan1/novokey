﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Delta.NovoKey
{
    public static class BackEnd
    {
        public static string Version
        {
            get
            {
                string version = "" + Assembly.GetExecutingAssembly().GetName().Version;
                return version;// version.Substring(0, version.Length - 2);
            }
        }

        // This fixes the PlatformID enumeration for MacOSX in Environment.OSVersion.Platform,
        // which is intentionally broken in Mono for historical reasons
        public static PlatformID Platform
        {
            get
            {
                IntPtr buf = IntPtr.Zero;

                try
                {
                    buf = Marshal.AllocHGlobal(8192);

                    if (uname(buf) == 0 && Marshal.PtrToStringAnsi(buf) == "Darwin")
                        return PlatformID.MacOSX;

                }
                catch
                {
                }
                finally
                {
                    if (buf != IntPtr.Zero)
                        Marshal.FreeHGlobal(buf);
                }

                return Environment.OSVersion.Platform;
            }
        }

        [DllImport("libc")]
        private static extern int uname(IntPtr buf);
    }
}
