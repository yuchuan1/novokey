﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delta.NovoKey
{
    /// <summary>
    /// RandomNumber
    /// </summary>
    public class RandomNumber
    {
        /// <summary>
        /// Gets the random number.
        /// </summary>
        /// <param name="seed">The seed.</param>
        /// <param name="min">The min.</param>
        /// <param name="max">The max.</param>
        /// <returns></returns>
        public int GetRandomNumber(uint seed, int min, int max)
        {
            var rng = new SimpleRNG();
            rng.SetSeed(seed);
            while (true)
            {
                double temp = rng.GetUniform() * Math.Pow(10, (GetIntegerDigitCount(seed)));
                int result = Convert.ToInt32(temp);

                if (result >= min && result <= max)
                    return result;
            }
        }

        /// <summary>
        /// Gets the integer digit count.
        /// </summary>
        /// <param name="valueInt">The value int.</param>
        /// <returns></returns>
        public static int GetIntegerDigitCount(uint valueInt)
        {
            double value = valueInt;
            int sign = 0;
            if (value < 0)
            {
                value = -value;
                sign = 1;
            }
            if (value <= 9)
            {
                return sign + 1;
            }
            if (value <= 99)
            {
                return sign + 2;
            }
            if (value <= 999)
            {
                return sign + 3;
            }
            if (value <= 9999)
            {
                return sign + 4;
            }
            if (value <= 99999)
            {
                return sign + 5;
            }
            if (value <= 999999)
            {
                return sign + 6;
            }
            if (value <= 9999999)
            {
                return sign + 7;
            }
            if (value <= 99999999)
            {
                return sign + 8;
            }
            if (value <= 999999999)
            {
                return sign + 9;
            }
            return sign + 10;
        }
    }
}
