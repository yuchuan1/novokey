﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Management;
using System.Security.Cryptography;
using System.Text;

namespace Delta.NovoKey
{
    /// <summary>
    /// Generates a 16 byte Unique Identification code of a computer
    /// Example: 4876-8DB5-EE85-69D3-FE52-8CF7-395D-2EA9
    /// </summary>
    public class FingerPrint
    {
        private static string _fingerPrint = string.Empty;
        private static string _output;
        private static string _error;

        /// <summary>
        /// Return a hardware identifier
        /// </summary>
        /// <returns></returns>
        public static string Value()
        {
            if (BackEnd.Platform == PlatformID.MacOSX)
            {
                //check if script exist, if not throw an exception
                const string shellScript = "uuid";
                var fi = new FileInfo(shellScript);
                if (fi.Exists)
                {
                    try
                    {
                        ExecuteShellCommand("sh", Environment.CurrentDirectory + "/" + shellScript, ref _output, ref _error);
                        _fingerPrint = _output.Trim();
                    }
                    catch (Exception)
                    {
                        
                        throw;
                    }
                }
                else
                {
                    throw new FileNotFoundException("Machine finger print script not found, cannot generate license!");
                }
            }
            else if (BackEnd.Platform == PlatformID.Win32NT || BackEnd.Platform == PlatformID.Win32S || BackEnd.Platform == PlatformID.Win32Windows || BackEnd.Platform == PlatformID.WinCE)
            {
                if (string.IsNullOrEmpty(_fingerPrint))
                {
                    _fingerPrint = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId());
                }
            }
            return _fingerPrint;
        }

        /// <summary>
        /// Execute a shell command
        /// </summary>
        /// <param name="_FileToExecute">File/Command to execute</param>
        /// <param name="_CommandLine">Command line parameters to pass</param> 
        /// <param name="_outputMessage">returned string value after executing shell command</param> 
        /// <param name="_errorMessage">Error messages generated during shell execution</param> 
        public static void ExecuteShellCommand(string _FileToExecute, string _CommandLine, ref string _outputMessage,
                                               ref string _errorMessage)
        {
            // Set process variable
            // Provides access to local and remote processes and enables you to start and stop local system processes.
            Process _Process = null;
            try
            {
                _Process = new Process();

                // invokes the cmd process specifying the command to be executed.
                //  string _CMDProcess = string.Format(System.Globalization.CultureInfo.InvariantCulture, @"{0}\cmd.exe", new object[] { Environment.SystemDirectory });

                // pass executing file to cmd (Windows command interpreter) as a arguments
                // /C tells cmd that we want it to execute the command that follows, and then exit.
                //   string _Arguments = string.Format(System.Globalization.CultureInfo.InvariantCulture, "", new object[] { _FileToExecute });
                string _CMDProcess = _FileToExecute;
                string _Arguments = _CommandLine;
                // pass any command line parameters for execution
                if (_CommandLine != null && _CommandLine.Length > 0)
                {
                    _Arguments += string.Format(CultureInfo.InvariantCulture, " {0}",
                                                new object[] {_CommandLine, CultureInfo.InvariantCulture});
                }

                // Specifies a set of values used when starting a process.
                var _ProcessStartInfo = new ProcessStartInfo(_CMDProcess, _Arguments);
                // sets a value indicating not to start the process in a new window. 
                _ProcessStartInfo.CreateNoWindow = true;
                // sets a value indicating not to use the operating system shell to start the process. 
                _ProcessStartInfo.UseShellExecute = false;
                // sets a value that indicates the output/input/error of an application is written to the Process.
                _ProcessStartInfo.RedirectStandardOutput = true;
                _ProcessStartInfo.RedirectStandardInput = true;
                _ProcessStartInfo.RedirectStandardError = true;
                _Process.StartInfo = _ProcessStartInfo;

                // Starts a process resource and associates it with a Process component.
                _Process.Start();

                // Instructs the Process component to wait indefinitely for the associated process to exit.
                _errorMessage = _Process.StandardError.ReadToEnd();
                _Process.WaitForExit();

                // Instructs the Process component to wait indefinitely for the associated process to exit.
                _outputMessage = _Process.StandardOutput.ReadToEnd();
                // _Process.WaitForExit();
            }
                //    catch (Win32Exception _Win32Exception)
                //    {
                //        // Error
                //        Console.WriteLine("Win32 Exception caught in process: {0}", _Win32Exception.ToString());
                //    }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception);
            }
            finally
            {
                // close process and do cleanup
                _Process.Close();
                _Process.Dispose();
                _Process = null;
            }
        }

        private static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            var enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return GetHexString(sec.ComputeHash(bt));
        }

        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                int n, n1, n2;
                n = b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char) (n2 - 10 + 'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char) (n1 - 10 + 'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1)%2 == 0) s += "-";
            }
            return s;
        }

        #region Original Device ID Getting Code

        /// <summary>
        /// Return a hardware identifier
        /// </summary>
        /// <param name="wmiClass"></param>
        /// <param name="wmiProperty"></param>
        /// <param name="wmiMustBeTrue"></param>
        /// <returns></returns>
        private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            var mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return result;
        }

        //Return a hardware identifier
        private static string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            var mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        private static string cpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }

        //BIOS Identifier
        private static string biosId()
        {
            return identifier("Win32_BIOS", "Manufacturer")
                   + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
                   + identifier("Win32_BIOS", "IdentificationCode")
                   + identifier("Win32_BIOS", "SerialNumber")
                   + identifier("Win32_BIOS", "ReleaseDate")
                   + identifier("Win32_BIOS", "Version");
        }

        //Main physical hard drive ID
        private static string diskId()
        {
            return identifier("Win32_DiskDrive", "Model")
                   + identifier("Win32_DiskDrive", "Manufacturer")
                   + identifier("Win32_DiskDrive", "Signature")
                   + identifier("Win32_DiskDrive", "TotalHeads");
        }

        //Motherboard ID
        private static string baseId()
        {
            return identifier("Win32_BaseBoard", "Model")
                   + identifier("Win32_BaseBoard", "Manufacturer")
                   + identifier("Win32_BaseBoard", "Name")
                   + identifier("Win32_BaseBoard", "SerialNumber");
        }

        //Primary video controller ID
        private static string videoId()
        {
            return identifier("Win32_VideoController", "DriverVersion")
                   + identifier("Win32_VideoController", "Name");
        }

        //First enabled network card ID
        private static string macId()
        {
            return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
        }

        #endregion
    }
}