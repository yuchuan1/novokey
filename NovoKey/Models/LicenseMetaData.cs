﻿using System;
using Delta.NovoKey.Exceptions;
using Newtonsoft.Json;

namespace Delta.NovoKey.Models
{
    public class LicenseMetaData
    {
        [JsonProperty(PropertyName = "Status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "ActivationStatus")]
        public bool ActivationStatus { get; set; }

        [JsonProperty(PropertyName = "RemainInstallations")]
        public int RemainInstallations { get; set; }

        [JsonProperty(PropertyName = "allowedMachines")]
        public int AllowedMachines { get; set; }

        [JsonProperty(PropertyName = "concurrentUsers")]
        public int NumberOfConcurrentUsers { get; set; }

        [JsonProperty(PropertyName = "TotalAccounts")]
        public int TotalAccounts { get; set; }

        [JsonProperty(PropertyName = "SubscriptionValidDays")]
        public int SubscriptionValidDays { get; set; }

        [JsonProperty(PropertyName = "SubscriptionExpDate")]
        public DateTime SubscriptionExpDate { get; set; }

        public static LicenseMetaData Parse(string json, bool IsActivate)
        {
            try
            {
                var metadata = JsonConvert.DeserializeObject<LicenseMetaData>(json);

                if (IsActivate)
                {
                    if (metadata.Status.Equals("NO_MORE_ACTIVATION"))
                    {
                        throw new NoMoreActivationException("You've reached maximum machine counts!");
                    }

                    if (metadata.Status.Equals("KEY_DOES_NOT_EXIST") || metadata.Status.Equals("KEY_IS_MISSING"))
                    {
                        throw new InvalidLicenseException("Server: Key not found!");
                    }

                    if (metadata.Status.Equals("INVALID_PRODUCTID"))
                    {
                        throw new InvalidProductKeyException("Invalid product ID specified!");
                    }
                }
                else
                {
                   if (metadata.Status.Equals("KEY_DOES_NOT_EXIST") || metadata.Status.Equals("KEY_IS_MISSING"))
                    {
                        throw new InvalidLicenseException("Server: Key not found!");
                    }

                    if (metadata.Status.Equals("INVALID_PRODUCTID"))
                    {
                        throw new InvalidProductKeyException("Invalid product ID specified!");
                    }
                    
                }
                

                return metadata;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
