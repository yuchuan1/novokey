﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace Delta.NovoKey.Models
{
    [ProtoContract]
    public class SubscriptionLicenseRecords
    {
        [ProtoMember(1)]
        public List<SubscriptionRecord> Subscriptions { get; set; }
    }

    [ProtoContract]
    public class SubscriptionRecord
    {
        [ProtoMember(2)]
        public string keyMD5 { get; set; }

        [ProtoMember(3)]
        public DateTime SubscriptionExpirationDate { get; set; }
    }
}
