﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Delta.NovoKey
{
    /// <summary>
    /// DES encryption
    /// </summary>
    public class DES
    {
        /// <summary>
        /// Encodes the specified DES.
        /// </summary>
        /// <param name="des">The DES.</param>
        /// <param name="value">The value.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <exception cref="System.IO.InvalidDataException">Key must be equal to 24 characters</exception>
        public static string Encode(TripleDES des, string value, string key)
        {
            if (key.Length != 24)
                throw new InvalidDataException("Key must be equal to 24 characters");

            byte[] keyBytes = Encoding.ASCII.GetBytes(key); //24characters        
            byte[] plainText = Encoding.ASCII.GetBytes(value);
            des.Key = keyBytes;
            des.Mode = CipherMode.CBC;
            ICryptoTransform ic = des.CreateEncryptor();
            byte[] enc = ic.TransformFinalBlock(plainText, 0, plainText.Length);
            string result = BitConverter.ToString(enc).Replace("-",string.Empty);
            return result;
        }

        /// <summary>
        /// Decodes the specified DES.
        /// </summary>
        /// <param name="des">The DES.</param>
        /// <param name="value">The value.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <exception cref="System.IO.InvalidDataException">Key must be equal to 24 characters</exception>
        public static string Decode(TripleDES des, string value, string key)
        {
            if (key.Length != 24)
                throw new InvalidDataException("Key must be equal to 24 characters");

            byte[] keyBytes = Encoding.ASCII.GetBytes(key); //24characters        
            byte[] plainText = Encoding.ASCII.GetBytes(value);
            des.Key = keyBytes;
            des.Mode = CipherMode.CBC;
            ICryptoTransform ic = des.CreateDecryptor();

            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptStream = new CryptoStream(memStream, ic, CryptoStreamMode.Write);
            byte[] data = CConvert.ConvertHexStringToByteArray(value);
            cryptStream.Write(data, 0, data.Length);
            cryptStream.FlushFinalBlock();
            memStream.Position = 0;
            byte[] result = new byte[Convert.ToInt32(memStream.Length - 1) + 1];
            memStream.Read(result, 0, Convert.ToInt32(result.Length));
            memStream.Close();
            memStream.Dispose();
            cryptStream.Close();
            cryptStream.Dispose();

            return Encoding.ASCII.GetString(result); ;
        }

       // static byte[] bytes = ASCIIEncoding.ASCII.GetBytes("012345678901234567890123");

        /// <summary>
        /// Encrypts the specified original string.
        /// </summary>
        /// <param name="originalString">The original string.</param>
        /// <param name="keyBytes">The key bytes.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">The string which needs to be encrypted can not be null.</exception>
        public static string Encrypt(string originalString, byte[] keyBytes)
        {
            if (String.IsNullOrEmpty(originalString))
            {
                throw new ArgumentNullException
                       ("The string which needs to be encrypted can not be null.");
            }

            TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(keyBytes, keyBytes), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        /// <summary>
        /// Decrypts the specified crypted string.
        /// </summary>
        /// <param name="cryptedString">The crypted string.</param>
        /// <param name="keyBytes">The key bytes.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">The string which needs to be decrypted can not be null.</exception>
        public static string Decrypt(string cryptedString, byte[] keyBytes)
        {
            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                   ("The string which needs to be decrypted can not be null.");
            }
            TripleDESCryptoServiceProvider cryptoProvider = new TripleDESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(keyBytes, keyBytes), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }

       
    }
}
