﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Delta.NovoKey.Exceptions;
using Delta.NovoKey.Models;
using ProtoBuf;
using RestSharp;

namespace Delta.NovoKey
{
    /// <summary>
    /// License class
    /// </summary>
    [XmlRoot("License", Namespace = "", IsNullable = false)]
    [Serializable]
    [ProtoContract]
    public class License
    {
        //public string DefaultLicenseFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private static string DefaultLicenseFilePath = "C:\\delta\\AppData\\License";
        private int ProductID {
            get { return GetLicenseInfoFromKey(ProductKey).ProductID; }
        }

        //const string LicenseServerIp = "172.16.5.34";
        const string LicenseServerIp = "service.novosync.com";
        // const string LicenseServerIp = "172.16.10.81:9018";
        private const string LicenseServerBaseUrl = "https://" + LicenseServerIp + "/key";
        private const string LicenseServerPingUrl = "https://" + LicenseServerIp + "/ping";

        private static string DefaultLicenseFile;

        [DllImport("wininet.dll", CharSet = CharSet.Auto)]
        static extern bool InternetGetConnectedState(ref ConnectionState lpdwFlags, int dwReserved);
        private static ConnectionState connectionState = 0;

        #region LicenseType enum
        /// <summary>
        /// License Type
        /// </summary>
        public enum LicenseType
        {
            /// <summary>
            /// Free license
            /// </summary>
            Free,

            /// <summary>
            /// Trial license
            /// </summary>
            Trial,

            /// <summary>
            /// Volume license
            /// </summary>
            Volume,

            /// <summary>
            /// Single license
            /// </summary>
            Single,

            /// <summary>
            /// Subscription license
            /// </summary>
            Subsctription,

            /// <summary>
            /// Invalid license
            /// </summary>
            Invalid
        }
        #endregion
        #region Status enum
        /// <summary>
        /// License Status
        /// </summary>
        public enum Status
        {
            NotFound,
            IsValid,
            IsValidTrialVersion,
            TrialExpired,
            Blacklisted,
            Invalid,
            InvalidMachineFingerPrint,
            SubscriptionExpired
        }
        #endregion

        private string _licenseGenerationRecordFile;

        private TrialLicenseGenerationRecord _trialLicense;
        /// <summary>
        /// License constructor
        /// </summary>
        public License()
        {
            Configuration config = null;
            string exeConfigPath = this.GetType().Assembly.Location;
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// License constructor
        /// </summary>
        /// <param name="licenseFile"></param>
        public License(int ProductID)
        {
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);

            var di = new DirectoryInfo(DefaultLicenseFilePath + "\\Delta Electronics");
            di.Create();

            var fi = new FileInfo(DefaultLicenseFile);
            if (fi.Exists)
            {
                License lic = ReadLicense(ProductID);
                KeyLicenseInfo licenseInfo = GetLicenseInfoFromKey(lic.ProductKey);
                Init(licenseInfo);
            }
            else
            {
                Type = LicenseType.Invalid;
                LicenseStatus = Status.NotFound;
            }
        }

        /// <summary>
        /// License constructor
        /// </summary>
        /// <param name="licenseInfo"></param>
        public License(KeyLicenseInfo licenseInfo)
        {
            Init(licenseInfo);
        }

        private static DateTime Today { get; set; }

        /// <summary>
        /// Product key
        /// </summary>
        [XmlElement]
        [ProtoMember(1)]
        public string ProductKey { get; set; }

        /// <summary>
        /// Machine finger print
        /// </summary>
        [ProtoMember(2)]
        public string MachineFingerPrint { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        [ProtoMember(3)]
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the activation date.
        /// </summary>
        /// <value>
        /// The activation date.
        /// </value>
        [XmlAttribute(DataType = "date")]
        [ProtoMember(4)]
        public DateTime ActivationDate { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        [XmlElement(DataType = "date")]
        [ProtoMember(5)]
        public static DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the signature.
        /// </summary>
        /// <value>
        /// The signature.
        /// </value>
        [ProtoMember(6)]
        public string Signature { get; set; }

        /// <summary>
        /// Gets or sets the license type.
        /// </summary>
        /// <value>
        /// license type.
        /// </value>
        [ProtoMember(7)]
        public LicenseType Type { get; set; }

        /// <summary>
        /// Gets or sets the license status.
        /// </summary>
        /// <value>
        /// The license status.
        /// </value>
        [ProtoMember(8)]
        public Status LicenseStatus { get; set; }

        /// <summary>
        /// flag to record whether the license had been activated on license server or not
        /// </summary>
        [ProtoMember(9)]
        public bool Activated { get; set; }

        [ProtoMember(10)]
        public string CustomerName { get; set; }

        [ProtoMember(11)]
        public string CustomerEmail { get; set; }

        // Used for NovoClassRoom
        // retrieve from license server
        [ProtoMember(12)]
        public int NumberOfConcurrentUsers { get; set; }

        // Used for NovoClassRoom
        // retrieve from license server
        [ProtoMember(13)]
        public int NumberOfAccounts { get; set; }

        public static KeyLicenseInfo GetLicenseInfoFromKey(string key)
        {
            var licenseInfo = new KeyLicenseInfo();
            string data = key.Substring(7, 4) + key.Substring(13, 4) + key.Substring(18, 4) + key.Substring(24, 4) +
                          key.Substring(30, 4);
            var keyData = new List<string>();
            for (int i = 0; i < data.Length; i = i + 2)
            {
                keyData.Add(data[i].ToString() + data[i + 1].ToString());
            }

            byte[] hiddenData = CConvert.ConvertFromKey(keyData);
            CConvert.ShiftRight(ref hiddenData);
            // byte[] unScrambledBytes = CConvert.UnScrambleData(hiddenData);

            byte[] proid = {hiddenData[0], hiddenData[1]};
            byte[] lictype = {hiddenData[2], hiddenData[3]};
            byte[] trialdays = {hiddenData[4], hiddenData[5]};
            byte[] numofcopies = {hiddenData[6], hiddenData[7]};
            byte[] checksum = {hiddenData[8], hiddenData[9]};

            licenseInfo.ProductKey = key.Trim();
            licenseInfo.ProductID = (short) (BitConverter.ToInt16(proid, 0) - 3420);
            licenseInfo.LicenseType = (LicenseType) ((BitConverter.ToInt16(lictype, 0) - 1571));
            licenseInfo.TrialDays = (short) (BitConverter.ToInt16(trialdays, 0) - 4725);
            licenseInfo.NumOfCopies = (short) (BitConverter.ToInt16(numofcopies, 0) - 7458);

            int checkSumFromKey = BitConverter.ToInt16(checksum, 0);
            int chk = licenseInfo.CheckSum();

            if (checkSumFromKey != chk)
                throw new InvalidCheckSumException("LicenseInfo CheckSum Failed! Suspect that the key was modified!");

            return licenseInfo;
        }

        private void Init(KeyLicenseInfo licenseInfo)
        {
            //trying to get datetime from ntp server
            //Today = GetTimeFromNTP();
            Today = DateTime.Now;

            MachineFingerPrint = FingerPrint.Value();
            ProductKey = licenseInfo.ProductKey;
            DefaultLicenseFile = GetDefaultLicenseFile(licenseInfo.ProductID);

            ActivationDate = Today;
            ExpirationDate = licenseInfo.TrialDays > 0 ? ActivationDate.AddDays(licenseInfo.TrialDays) : DateTime.Now.AddYears(100);

            switch (licenseInfo.LicenseType)
            {
                case LicenseType.Subsctription:
                    Type = LicenseType.Subsctription;
                    break;
                case LicenseType.Free:
                    Type = LicenseType.Free;
                    break;
                case LicenseType.Single:
                    Type = LicenseType.Single;
                    break;
                case LicenseType.Trial:
                    Type = LicenseType.Trial;
                    var trialLicenseRecord = ReadTrialLicenseRecord(licenseInfo.ProductID);
                    if (trialLicenseRecord == null)
                    {
                        try
                        {
                            SaveTrialLicenseRecord(true, licenseInfo.ProductKey);
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                    }
                    break;
                case LicenseType.Volume:
                    Type = LicenseType.Volume;
                    break;
            }

           // byte[] encKey = Encoding.ASCII.GetBytes(MachineFingerPrint).Take(24).ToArray();

            string keyString = ProductKey;
            while (keyString.Length < 24)
            {
                keyString += keyString;
            }

            keyString = new string(keyString.Take(24).ToArray());

            byte[] keyBytes = Encoding.ASCII.GetBytes(keyString);

            Signature = DES.Encrypt(ProductKey + MachineFingerPrint + ProductName + ActivationDate, keyBytes);
            LicenseStatus = GetStatus();
        }

        public DateTime GetSubscriptionExpirationDate()
        {
            if(Type != LicenseType.Subsctription)
                throw new NovoKeyException("Subscription Expiration Date only applies to Subscription license");

            var expDate = new DateTime();
            foreach (var item in ReadSubscriptionRecords().Subscriptions)
            {
                if (item.keyMD5 == CalculateMD5Hash(ProductKey))
                    expDate = item.SubscriptionExpirationDate;
            }

            if (expDate.Equals(new DateTime(1980, 1, 1)))
            {
                if(!Activated)
                    throw new NovoKeyException("License has to be activated before getting an expiration date");
            }

            return expDate;
           // throw new NovoKeyException("Error calulating expiration date!");
        }

        /// <summary>
        /// Gets the trial days left.
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetTrialDaysLeft()
        {
            string keymd5 = CalculateMD5Hash(ProductKey);

            foreach (var item in ReadTrialLicenseRecord(ProductID).TrialProductKeysUsed)
            {
                if (item.keyMD5.Equals(keymd5))
                    ExpirationDate = item.ExpirationDate;
            }
            
         //   var emptyDate = new DateTime();

         //   if (Today == emptyDate)
                //Today = GetTimeFromNTP();
                Today = DateTime.Now.AddDays(-1);

            TimeSpan timeSpan = ExpirationDate - Today;

            return timeSpan;

            //if (Type == LicenseType.Trial)
            //{
            //    TimeSpan timeSpan = ExpirationDate - Today;
            //    return timeSpan;
            //}
            //else
            //{
            //    TimeSpan timeSpan = DateTime.Now.AddYears(1) - Today;
            //    return timeSpan;
            //}
        }

        /// <summary>
        /// Gets the time from NTP.
        /// </summary>
        /// <returns></returns>
        public DateTime GetTimeFromNTP()
        {
            DateTime result;
            if (IsNetworkAvailable(10000000))
            {
                try
                {
                    result = NtpClient.GetNetworkTime();
                }
                catch (Exception)
                {
                    try
                    {
                        //trying to get time from domain server
                        string domainName =
                            IPGlobalProperties.GetIPGlobalProperties().DomainName;
                        if (domainName != String.Empty)
                        {
                            var context = new DirectoryContext(DirectoryContextType.DirectoryServer, domainName);
                            DomainController dc = DomainController.GetDomainController(context);
                            result = dc.CurrentTime;
                        }
                        else
                        {
                            result = DateTime.Now;
                        }
                        return result;
                    }
                    catch (Exception)
                    {
                        //if both getting time from NTP servers and domain controller fails, use system time
                        return DateTime.Now;
                    }
                }
            }
            else
            {
                return DateTime.Now;
            }

            return result;
        }

        /// <summary>
        /// Indicates whether any network connection is available.
        /// Filter connections below a specified speed, as well as virtual network cards.
        /// </summary>
        /// <param name="minimumSpeed">The minimum speed required. Passing 0 will not filter connection using speed.</param>
        /// <returns>
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNetworkAvailable(long minimumSpeed)
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return false;

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                // discard because of standard reasons
                if ((ni.OperationalStatus != OperationalStatus.Up) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Loopback) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Tunnel))
                    continue;

                // this allow to filter modems, serial, etc.
                // I use 10000000 as a minimum speed for most cases
                if (ni.Speed < minimumSpeed)
                    continue;

                // discard virtual cards (virtual box, virtual pc, etc.)
                if ((ni.Description.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (ni.Name.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0))
                    continue;

                // discard "Microsoft Loopback Adapter", it will not show as NetworkInterfaceType.Loopback but as Ethernet Card.
                if (ni.Description.Equals("Microsoft Loopback Adapter", StringComparison.OrdinalIgnoreCase))
                    continue;

                return true;
            }
            return false;
        }

        private Status GetStatus()
        {
            return GetStatus(WebRequest.DefaultWebProxy);
        }

        private Status GetStatus(string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            return GetStatus(proxy);
        }
        
        /// <summary>
        /// Get current license status
        /// </summary>
        /// <returns></returns>
        private Status GetStatus(IWebProxy proxy)
        {
            try
            {
                if (Type == LicenseType.Invalid || LicenseStatus == Status.Invalid)
                    return Status.Invalid;

                string keyString = ProductKey;
                while (keyString.Length < 24)
                {
                    keyString += keyString;
                }

                keyString = new string(keyString.Take(24).ToArray());
                byte[] keyBytes = Encoding.ASCII.GetBytes(keyString);
                if (
                    DES.Encrypt(
                        ProductKey + MachineFingerPrint + ProductName + ActivationDate,
                        keyBytes) != Signature)
                    return Status.Invalid;

                if (MachineFingerPrint != FingerPrint.Value())
                    return Status.InvalidMachineFingerPrint;

                if (Type == LicenseType.Trial)
                {
                    TimeSpan trialDaysLeft = GetTrialDaysLeft();
                    if (trialDaysLeft.TotalDays >= 0)
                        return Status.IsValidTrialVersion;

                    if (trialDaysLeft.TotalDays < 0)
                        return Status.TrialExpired;
                }

                var result = Validate(ProductKey, false);
                if (result.Status.Equals("Internet connection failed"))
                    return LicenseStatus;
                
                if (result.Status.Equals("AVAILABLE_KEY"))
                {
                    this.NumberOfAccounts = result.TotalAccounts;
                    this.NumberOfConcurrentUsers = result.NumberOfConcurrentUsers;
                    this.LicenseStatus = Status.IsValid;
                    //this.Activated = result.ActivationStatus;
                    this.Save();
                }

                if (result.Status.Equals("PROHIBITED_KEY"))
                {
                    this.LicenseStatus = Status.Blacklisted;
                    this.Save();
                    return Status.Blacklisted;
                }

                if (result.Status.Equals("KEY_IS_MISSING") || result.Status.Equals("INVALID_PRODUCTID"))
                {
                    this.LicenseStatus = Status.Invalid;
                    this.Save();
                    return Status.Invalid;
                }
            }
            catch (Exception)
            {
                //return Status;
            }

            return Status.IsValid;
        }

        public void Save()
        {
            if (Type == LicenseType.Trial)
                SaveTrialLicenseFile(ProductID);
            else
            {
                SaveLicenseFile(ProductID);
            }
        }

        //public void Save(int ProductID)
        //{
        //    if (Type == LicenseType.Trial)
        //        SaveTrialLicenseFile(licenseFile);
        //    else
        //    {
        //        SaveLicenseFile(licenseFile);
        //    }
        //}

        /// <summary>
        /// Saves the license file.
        /// </summary>
        //private void SaveLicenseFile()
        //{
        //    SaveLicenseFile(null);
        //}

        //private void SaveTrialLicenseFile()
        //{
        //    SaveTrialLicenseFile(string.Empty);
        //}

        /// <summary>
        /// Saves the license file.
        /// </summary>
        /// <param name="licenseFileName">Name of the license file.</param>
        private void SaveTrialLicenseFile(int ProductID)
        {
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);

            // check trial license record to see if we can generate a license or not
            var licenseRecord = ReadTrialLicenseRecord(ProductID);
            var keyMD5 = CalculateMD5Hash(ProductKey);

            bool found = false;
            foreach (var item in licenseRecord.TrialProductKeysUsed)
            {
                if (item.keyMD5 == keyMD5)
                {
                   if(item.ExpirationDate.Date <= Today.Date)
                    found = true;
                }
            }

            if(licenseRecord != null)
                if (licenseRecord.CanGenerateTrialLicense || !found)
                {
                    DefaultLicenseFile = GetDefaultLicenseFile(ProductID);
                    using (FileStream file = File.Create(DefaultLicenseFile))
                    {
                        Serializer.Serialize(file, this);
                        file.Close();
                        file.Dispose();
                    }
                    SaveTrialLicenseRecord(false, ProductKey);
                }
                else
                {
                    throw new TrialKeyExpiredException("Trial license can only be generated once! Cannot generate another one!");
                }
        }

        /// <summary>
        /// Saves the license file.
        /// </summary>
        /// <param name="licenseFileName">Name of the license file.</param>
        private void SaveLicenseFile(int ProductID)
        {
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);

            var fi = new FileInfo(DefaultLicenseFile);
            if(fi.Exists)
                fi.Delete();

            using (FileStream file = File.Create(DefaultLicenseFile))
                    {
                        Serializer.Serialize(file, this);
                        file.Close();
                        file.Dispose();
                    }
        }

        public TrialLicenseGenerationRecord ReadTrialLicenseRecord(int ProductID)
        {
            _licenseGenerationRecordFile = GetDefaultTrialLicenseRecordFile(ProductID);

            var fi = new FileInfo(_licenseGenerationRecordFile);
            if(fi.Exists)
            {
                TrialLicenseGenerationRecord licenseRecord;
                using (var file = File.OpenRead(fi.FullName))
                {
                    licenseRecord = Serializer.Deserialize<TrialLicenseGenerationRecord>(file);
                    return licenseRecord;
                }
            }
            else
            {
                return null;
            }
        }

        private void SaveTrialLicenseRecord(bool canGenerateTrialLicense, string productKey)
        {
            KeyLicenseInfo info = GetLicenseInfoFromKey(productKey);
            _licenseGenerationRecordFile = GetDefaultTrialLicenseRecordFile(info.ProductID);
            
            var record = ReadTrialLicenseRecord(ProductID);

            var item = new TrialKeyRecord();
            item.keyMD5 = CalculateMD5Hash(productKey);
            item.ExpirationDate = ExpirationDate;

            if (record == null)
            {
                List<TrialKeyRecord> keys = new List<TrialKeyRecord>();
                keys.Add(item);

                _trialLicense = new TrialLicenseGenerationRecord();
                _trialLicense.CanGenerateTrialLicense = canGenerateTrialLicense;
                _trialLicense.TrialProductKeysUsed = keys;

                using (
                    FileStream file =
                        File.Create(_licenseGenerationRecordFile))
                {
                    Serializer.Serialize(file, _trialLicense);
                }
            }
            else
            {
                record.CanGenerateTrialLicense = canGenerateTrialLicense;
                record.TrialProductKeysUsed.Add(item);
                
                using (
                    FileStream file =
                        File.Create(_licenseGenerationRecordFile))
                {
                    Serializer.Serialize(file, record);
                }
            }

        }

        public static SubscriptionLicenseRecords ReadSubscriptionRecords()
        {
            var defaultSubscriptionRecordFile = GetDefaultSubscriptionRecordFile();
            var fi = new FileInfo(defaultSubscriptionRecordFile);
            if (fi.Exists)
            {
                SubscriptionLicenseRecords records;
                using (var file = File.OpenRead(fi.FullName))
                {
                    return Serializer.Deserialize<SubscriptionLicenseRecords>(file);
                }
            }

            return null;
        }

        private static void SaveSubscriptionRecord(License lic, LicenseMetaData metadata)
        {
            var expDate = new DateTime(1980, 1, 1);

            if (metadata.SubscriptionValidDays > 0)
            {
                // calculate expiration date
                expDate = DateTime.Now.Date.AddDays(metadata.SubscriptionValidDays);
            }
            else
            {
                // set expiration date to metadata.SubscriptionExpDate
                expDate = metadata.SubscriptionExpDate;
            }

                //var records = ReadSubscriptionRecords(lic.ProductKey);

                //if (records != null)
                //{
                //    foreach (var item in records.Subscriptions)
                //    {
                //        if (item.keyMD5 == CalculateMD5Hash(lic.ProductKey))
                //            item.SubscriptionExpirationDate = expDate;
                //    }
                //}
                //else
                //{
                //    records = new SubscriptionLicenseRecords();
                //    var record = new SubscriptionRecord
                //    {
                //        SubscriptionExpirationDate = expDate,
                //        keyMD5 = CalculateMD5Hash(lic.ProductKey)
                //    };

                //    records.Subscriptions.Add(record);

                //}

                // save expiration date to a file somewhere...
            
            var defaultSubscriptionRecordFile = GetDefaultSubscriptionRecordFile();

            var licenseRecord = new SubscriptionLicenseRecords();
            var record = new SubscriptionRecord
            {
                keyMD5 = CalculateMD5Hash(lic.ProductKey),
                SubscriptionExpirationDate = expDate
            };

            var fi = new FileInfo(defaultSubscriptionRecordFile);

            if ( !fi.Exists )
            {
                var lists = new List<SubscriptionRecord>();
                lists.Add(record);

                licenseRecord.Subscriptions = lists;
                using (
                    FileStream file =
                        File.Create(defaultSubscriptionRecordFile))
                {
                    Serializer.Serialize(file, licenseRecord);
                }
            }
            else
            {
                SubscriptionLicenseRecords records;
                var recordsToDelete = new List<SubscriptionRecord>();
                using (var file = File.OpenRead(defaultSubscriptionRecordFile))
                {
                    records = Serializer.Deserialize<SubscriptionLicenseRecords>(file);
                    foreach (var item in records.Subscriptions)
                    {
                        if (item.keyMD5 == CalculateMD5Hash(lic.ProductKey))
                            recordsToDelete.Add(item);
                    }
                }

                foreach (var item in recordsToDelete)
                {
                    records.Subscriptions.Remove(item);
                }

                fi = new FileInfo(defaultSubscriptionRecordFile);
                fi.Delete();

                var lists = new List<SubscriptionRecord>();
                lists.Add(record);

                licenseRecord.Subscriptions = lists;
                using (
                    FileStream file =
                        File.Create(defaultSubscriptionRecordFile))
                {
                    Serializer.Serialize(file, licenseRecord);
                }
                
            }
           
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        /// <summary>
        /// Reads the license with default licence file name: license.lic
        /// </summary>
        /// <returns></returns>
        //public static License ReadLicense()
        //{
        //    return ReadLicense(DefaultLicenseFile);
        //}

        // read local license file without network connection
        public static License ReadLicense(int ProductID)
        {
           //return ReadLicense(ProductID, WebRequest.DefaultWebProxy);
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);

            var fi = new FileInfo(DefaultLicenseFile);
            if (fi.Exists)
            {
                License lic;
                using (FileStream file = File.OpenRead(DefaultLicenseFile))
                {
                    lic = Serializer.Deserialize<License>(file);
                }

                if (lic.MachineFingerPrint != FingerPrint.Value())
                    throw new InvalidLicenseException("Machine fnigerprint mismatch!");

                try
                {
                    lic.LicenseStatus = lic.GetStatus();
                }
                catch (Exception)
                {
                    
                }

                


                if (lic.Type == LicenseType.Trial)
                {
                    if (lic.GetTrialDaysLeft().Milliseconds < 0)
                    {
                        lic.LicenseStatus = Status.TrialExpired;
                        lic.SaveLicenseFile(lic.ProductID);
                        
                        //throw new TrialKeyExpiredException();
                    }
                }

                return lic;
            }
            else
            {
                //throw new LicenseNotFoundException();
                var lic = new License();
                lic.Type = LicenseType.Invalid;
                lic.LicenseStatus = Status.NotFound;
                return lic;
            }
        }

        /// <summary>
        /// Reads the license.
        /// </summary>
        /// <param name="licenseFile">The full path of license file.</param>
        /// <returns></rehturns>
        public static License ReadLicense(int ProductID, IWebProxy proxy)
        {
            if(proxy.Credentials == null)
                proxy.Credentials = CredentialCache.DefaultCredentials;

            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);
           
            var fi = new FileInfo(DefaultLicenseFile);
            if (fi.Exists)
            {
                License lic;
                using (var file = File.OpenRead(DefaultLicenseFile))
                {
                    lic = Serializer.Deserialize<License>(file);
                }
                try
                {
                    lic.LicenseStatus = lic.GetStatus(proxy);
                }
                catch (Exception)
                {
                    
                }
               
                return lic;
            }
            else
            {
               // throw new LicenseNotFoundException();
                var lic = new License();
                lic.Type = LicenseType.Invalid;
                lic.LicenseStatus = Status.NotFound;
                return lic;
            }
            
          //  throw new LicenseNotFoundException();
        }

        public static License ReadLicense(int ProductID, string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            var proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            return ReadLicense(ProductID, proxy);
        }

        // for c++

        /// <summary>
        /// Gets the type of the license. For C++ calling purpose
        /// </summary>
        /// <returns></returns>
        //public static LicenseType GetLicenseType()
        //{
        //    return GetLicenseType(DefaultLicenseFile);
        //}

        /// <summary>
        /// Gets the type of the license. For C++ calling purpose
        /// </summary>
        /// <param name="licenseFile">The license file.</param>
        /// <returns></returns>
        public static LicenseType GetLicenseType(int ProductID)
        {
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);
            var lic = new License();
            var fi = new FileInfo(DefaultLicenseFile);

            if (fi.Exists)
                using (FileStream file = File.OpenRead(DefaultLicenseFile))
                {
                    lic = Serializer.Deserialize<License>(file);
                }
            else
                lic.Type = LicenseType.Invalid;

            return lic.Type;
        }

        public static LicenseMetaData Validate(string key, bool IsActivate)
        {
            return Validate(key,  IsActivate, WebRequest.DefaultWebProxy);
        }
        
        public static LicenseMetaData Validate(string key, bool IsActivate, IWebProxy proxy)
        {
           // WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultCredentials;
            if (proxy.Credentials == null)
                proxy.Credentials = CredentialCache.DefaultCredentials;

            KeyLicenseInfo info = GetLicenseInfoFromKey(key);

            var failedResponse = new LicenseMetaData();

            // check if the computer is connected to the internet
           // string isConnectedToInternet = InternetGetConnectedState(ref connectionState, 0).ToString();

            //if (isConnectedToInternet.Equals("True"))

                if (PingLicenseServer.Ping(proxy))
                {
                    IRestClient client = new RestClient
                    {
                        BaseUrl = LicenseServerBaseUrl
                    };
                    client.Proxy = proxy;

                    var request = new RestRequest { Method = Method.GET };
                    request.AddParameter("keyValue", key);
                    request.AddParameter("productID", info.ProductID);

                    try
                    {
                        var response = (RestResponse)client.Execute(request);

                        if (response.ResponseStatus != ResponseStatus.Completed)
                        {
                            throw new LicenseServerErrorException(response.ErrorMessage);
                        }

                        if (response.ResponseStatus == ResponseStatus.Completed)
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                return LicenseMetaData.Parse(response.Content, false);
                            }
                            else
                            {
                                throw new LicenseServerErrorException(response.ErrorMessage);
                            }
                        }
                       
                        throw new LicenseServerErrorException(response.ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        //failedResponse.Status = ex.Message;
                        throw;// new LicenseServerErrorException(ex.Message);
                    }
                }

            throw new ServerUnreachableException();
        }

        public static bool ValidateLicense(int ProductID)
        {
            return ValidateLicense(ProductID, String.Empty, String.Empty, false);
        }

        public static bool ValidateLicense(int ProductID, string customerName, string customerEmail)
        {
            return ValidateLicense(ProductID, customerName, customerEmail, true);
        }

        /// <summary>
        /// Validates the license. 
        /// </summary>
        /// <returns></returns>
        //public static bool ValidateLicense(string customerName, string customerEmail, bool tryActivateKey)
        //{
        //    return ValidateLicense(DefaultLicenseFile, customerName, customerEmail, tryActivateKey);
        //}

        /// <summary>
        /// Validates the license with spedified license file
        /// </summary>
        /// <param name="licenseFile">Full path of the license file</param>
        /// <param name="customerName"></param>
        /// <param name="customerEmail"></param>
        /// <returns></returns>
        public static bool ValidateLicense(int ProductID, string customerName, string customerEmail, bool tryActivateKey)
        {
            DefaultLicenseFile = GetDefaultLicenseFile(ProductID);

            License lic = ReadLicense(ProductID);
            if (lic.LicenseStatus != Status.NotFound)
            {
                // check if the key is expired
                if (lic.GetTrialDaysLeft().Days < 1)
                {
                    lic.LicenseStatus = Status.TrialExpired;
                    lic.Save();
                    return false;
                }
                // check if the key is revoked
                //if (IsRevoked(lic.ProductKey))
                var metadata = Validate(lic.ProductKey,false);
                lic = ReadLicense(ProductID);
                if (metadata.Status.Equals("PROHIBITED_KEY"))
                {
                    lic.LicenseStatus = Status.Blacklisted;
                    lic.Save();
                    return false;
                }
                if (metadata.Status.Equals("KEY_IS_MISSING") || metadata.Status.Equals("INVALID_PRODUCTID"))
                {
                    lic.LicenseStatus = Status.Invalid;
                    lic.Save();
                    return false;
                }

                //check is license is activated, if not, try activating it
                if (tryActivateKey)
                if (!lic.Activated)
                {
                    if (Activate(ProductID))
                    {
                        lic = ReadLicense(ProductID);
                        if (lic.Type != LicenseType.Invalid)
                        {
                            if (lic.Type == LicenseType.Trial)
                                lic.SaveTrialLicenseFile(ProductID);
                            else
                            {
                                lic.SaveLicenseFile(ProductID);
                            }
                        }
                    }
                }
            
                Status state;
                try
                {
                    state = lic.GetStatus();
                    lic = ReadLicense(ProductID);
                }
                catch (Exception ex)
                {
                    return false;
                }

                if (lic.Type != LicenseType.Invalid)
                    switch (state)
                    {
                        case Status.IsValid:
                        case Status.IsValidTrialVersion:
                            return true;

                        case Status.Blacklisted:
                            throw new KeyRevokedException("Your product key had been revoked.");

                        case Status.Invalid:
                            throw new InvalidProductKeyException("Your product key is invalid.");

                        case Status.InvalidMachineFingerPrint:
                            throw new InvalidMachineFingerPrintException("Machine fingerprint mismatch!");

                        case Status.NotFound:
                            throw new LicenseNotFoundException("License not found!");

                        case Status.TrialExpired:
                            throw new TrialKeyExpiredException("Trial license is expired");

                        case Status.SubscriptionExpired:
                            throw new SubscriptionExpiredException("Subscription license is expired");
                    }
                else 
                    return false;
            }
            return false;
            
        }

        //private static bool Ping()
        //{
        //    IRestClient client = new RestClient
        //    {
        //        BaseUrl = LicenseServerPingUrl
        //    };

        //    var request = new RestRequest { Method = Method.GET };

        //    try
        //    {
        //        var response = (RestResponse)client.Execute(request);

        //        if (response.ResponseStatus != ResponseStatus.Completed)
        //            return false;

        //        if (response.ResponseStatus == ResponseStatus.Completed)
        //        {
        //            if (response.StatusCode == HttpStatusCode.OK)
        //            {
        //                if (response.Content == "1")
        //                {
        //                    return true;
        //                }
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        return false;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //}

        public static LicenseMetaData ActivateLicenseKey(KeyLicenseInfo info, string customerName, string customerEmail,
                                                         string machineFingerprint)
        {
            return ActivateLicenseKey(info, customerName, customerEmail, machineFingerprint,
                               WebRequest.DefaultWebProxy);
        }

        public static LicenseMetaData ActivateLicenseKey(KeyLicenseInfo info, string customerName, string customerEmail,
                                                         string machineFingerprint, string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);

            return ActivateLicenseKey(info, customerName, customerEmail, machineFingerprint,
                               proxy);
            
            
        }

        /// <summary>
        /// Activate license key
        /// </summary>
        /// <param name="info"></param>
        /// <param name="customerName"></param>
        /// <param name="customerEmail"></param>
        /// <param name="machineFingerprint"></param>
        /// <returns></returns>
        public static LicenseMetaData ActivateLicenseKey(KeyLicenseInfo info, string customerName, string customerEmail, string machineFingerprint, IWebProxy proxy)
        {
            var failedResponse = new LicenseMetaData();
            
            // check if the computer is connected to the internet
            string isConnectedToInternet = InternetGetConnectedState(ref connectionState, 0).ToString();
            // bool pingSuccess = false;

            if (isConnectedToInternet.Equals("True"))
            {
                if (PingLicenseServer.Ping(proxy))
                {
                    IRestClient client = new RestClient
                    {
                        BaseUrl = LicenseServerBaseUrl
                    };
                    client.Proxy = proxy;
                    if (proxy.Credentials == null)
                        client.Proxy.Credentials = CredentialCache.DefaultCredentials;


                    var request = new RestRequest { Method = Method.POST };
                    request.AddParameter("keyValue", info.ProductKey);
                    request.AddParameter("productID", info.ProductID);
                    request.AddParameter("machineFingerPrintKey", machineFingerprint);
                    request.AddParameter("customerName", customerName);
                    request.AddParameter("customerEmail", customerEmail);

                    var response = (RestResponse)client.Execute(request);

                    if (response.ResponseStatus != ResponseStatus.Completed)
                    {
                        failedResponse.Status = response.ErrorMessage;
                        return failedResponse;
                    }

                    if (response.ResponseStatus == ResponseStatus.Completed)
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            return LicenseMetaData.Parse(response.Content, true);
                        }
                        else
                        {
                            failedResponse.Status = response.ErrorMessage;
                            return failedResponse;
                        }
                    }
                    failedResponse.Status = response.ErrorMessage;
                    return failedResponse;
                }
            }
            failedResponse.Status = "CONNCECTION_ERROR";
            return failedResponse;
        }

        public static bool Activate(int ProductID)
        {
            return Activate(ProductID, WebRequest.DefaultWebProxy);
        }

        public static bool Activate(int ProductID, string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            return Activate(ProductID, proxy);
        }

        /// <summary>
        /// Try to activate current license if it exists
        /// </summary>
        /// <returns></returns>
        public static bool Activate(int ProductID, IWebProxy proxy)
        {
            License lic = ReadLicense(ProductID);

            if (lic.LicenseStatus != Status.NotFound)
            {
                if (PingLicenseServer.Ping())
                {
                    // check if the license is activated, if not, try to send activation to server
                    var metadata = ActivateLicenseKey(GetLicenseInfoFromKey(lic.ProductKey),
                                                              lic.CustomerName,
                                                              lic.CustomerEmail, FingerPrint.Value());

                    if (metadata.Status.Equals("SUCCESS"))
                    {
                        lic.Activated = true;
                        lic.NumberOfConcurrentUsers = metadata.NumberOfConcurrentUsers;
                        lic.NumberOfAccounts = metadata.TotalAccounts;
          
                        lic.Save();

                        if (lic.Type == LicenseType.Subsctription)
                            SaveSubscriptionRecord(lic, metadata);

                        
                        
                        return true;
                    }
                    else if (metadata.Status.Equals("PROHIBITED_KEY"))
                    {
                        lic.LicenseStatus = Status.Blacklisted;
                        lic.Save();
                        return false;
                    }
                    else if (metadata.Status.Equals("KEY_IS_MISSING") || metadata.Status.Equals("INVALID_PRODUCTID"))
                    {
                        lic.LicenseStatus = Status.Invalid;
                        lic.Save();
                        return false;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string GetDefaultLicenseFile(int ProductID)
        {
            switch (ProductID)
            {
                case 1:
                    DefaultLicenseFile = DefaultLicenseFilePath +
                                         "\\Delta Electronics\\NovoTeach.lic";
                    break;

                case 2:
                    DefaultLicenseFile = DefaultLicenseFilePath +
                                         "\\Delta Electronics\\NovoClass.lic";
                    break;
                default:
                    throw new NovoKeyException("Invalid product id:" + ProductID);
                    break;
            }

            return DefaultLicenseFile;
        }

        public static string GetDefaultTrialLicenseRecordFile(int ProductID)
        {
            switch (ProductID)
            {
                case 1:
                    DefaultLicenseFile = DefaultLicenseFilePath +
                                         "\\Delta Electronics\\NovoTeach.dll";
                    break;

                case 2:
                    DefaultLicenseFile = DefaultLicenseFilePath +
                                         "\\Delta Electronics\\NovoClass.dll";
                    break;
                default:
                    throw new NovoKeyException("Invalid product id:" + ProductID);
                    break;
            }

            return DefaultLicenseFile;
        }

        public static string GetDefaultSubscriptionRecordFile()
        {
            string DefaultFile = DefaultLicenseFilePath +
                                         "\\Delta Electronics\\system.bin";
           
            return DefaultFile;
        }

        public static int AvailableInstalations(int ProductID)
        {
            return AvailableInstalations(ProductID, WebRequest.DefaultWebProxy);
        }

        public static int AvailableInstalations(int ProductID, string proxyAddress, int proxyPort, string proxyUser, string proxyPassword, string proxyDomain)
        {
            WebProxy proxy = new WebProxy(proxyAddress, proxyPort);
            proxy.Credentials = new NetworkCredential(proxyUser, proxyPassword, proxyDomain);
            return AvailableInstalations(ProductID, proxy);
        }

        public static int AvailableInstalations(int ProductID,IWebProxy proxy)
        {
            License lic = ReadLicense(ProductID);
            if (lic.LicenseStatus != Status.NotFound)
            {
                if (PingLicenseServer.Ping(proxy))
                {
                    var metadata = Validate(lic.ProductKey, false, proxy);
                    return metadata.RemainInstallations;
                }
                else
                {
                    throw new WebException("ERROR CONNECTING TO INTERNET");
                }
            }
            else
            {
                throw new LicenseNotFoundException();
            }
        }
    }
}