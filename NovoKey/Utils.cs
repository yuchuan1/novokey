﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Delta.NovoKey
{
    /// <summary>
    /// Utilities
    /// </summary>
    public class Utils
    {
        [DllImport("wininet.dll")]
        private static extern bool InternetGetConnectedState(out int Description, int ReservedValue);

        /// <summary>
        /// Determines whether [is connected to internet].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is connected to internet]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }

        /// <summary>
        /// Pings the host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <returns></returns>
        public static bool PingHost(string host)
        {
            //string to hold our return messge
            string returnMessage = string.Empty;

            //IPAddress instance for holding the returned host
            IPAddress address = Dns.GetHostAddresses(host)[0];

            //set the ping options, TTL 128
            var pingOptions = new PingOptions(128, true);

            //create a new ping instance
            var ping = new Ping();

            //32 byte buffer (create empty)
            var buffer = new byte[32];
            bool result = false;

            //first make sure we actually have an internet connection
            if (IsConnectedToInternet())
            {
                //here we will ping the host 3 times (standard)
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        //send the ping 4 times to the host and record the returned data.
                        //The Send() method expects 4 items:
                        //1) The IPAddress we are pinging
                        //2) The timeout value
                        //3) A buffer (our byte array)
                        //4) PingOptions
                        PingReply pingReply = ping.Send(address, 500, buffer, pingOptions);

                        //make sure we dont have a null reply
                        if (!(pingReply == null))
                        {
                            switch (pingReply.Status)
                            {
                                case IPStatus.Success:
                                    // returnMessage = string.Format("Reply from {0}: bytes={1} time={2}ms TTL={3}", pingReply.Address, pingReply.Buffer.Length, pingReply.RoundtripTime, pingReply.Options.Ttl);
                                    result = true;
                                    break;
                                case IPStatus.TimedOut:
                                    //returnMessage = "Connection has timed out...";
                                    result = false;
                                    break;
                                default:
                                    // returnMessage = string.Format("Ping failed: {0}", pingReply.Status.ToString());
                                    result = false;
                                    break;
                            }
                        }
                    }
                    catch (PingException ex)
                    {
                        // returnMessage = string.Format("Connection Error: {0}", ex.Message);
                        return false;
                    }
                    catch (SocketException ex)
                    {
                        // returnMessage = string.Format("Connection Error: {0}", ex.Message);
                        return false;
                    }

                    return result;
                }

                return result;
            }
            return result;
        }
    }
}