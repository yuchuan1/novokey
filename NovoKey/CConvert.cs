﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Delta.NovoKey
{

    // Various conversions from one value to another.
    // Hex conversions aren't actually used. =]

    /// <summary>
    /// Various conversions from one value to another.
    /// </summary>
    public static class CConvert
    {
        /// <summary>
        /// Conver to hex to long
        /// </summary>
        /// <param name="Hex">The hex.</param>
        /// <returns></returns>
        public static long FromHex(string Hex)
        {
            return Int64.Parse(Hex, NumberStyles.HexNumber);
        }

        /// <summary>
        /// Convert long To hex.
        /// </summary>
        /// <param name="Deci">value</param>
        /// <returns></returns>
        public static string ToHex(long Deci)
        {
            return Deci.ToString("X");
        }

        /// <summary>
        /// Convert a hex string to a byte array. 
        /// </summary>
        /// <param name="hexString">Hex string. The number of characters must be even number.</param>
        /// <returns></returns>
        public static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = Byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }

        
        /// <summary>
        /// Convert Base36 string into long
        /// </summary>
        /// <param name="IBase36"></param>
        /// <returns></returns>
        public static long FromBase36(string IBase36)
        {
            IBase36 = IBase36.ToUpper();
            string[] Base36 =
                {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
                };
            int i = 0;
            long v = 0;
            for (i = IBase36.Length - 1; i >= 0; i--)
            {
                long bc = (long)Math.Pow(36, ((IBase36.Length - 1) - i));
                if (Base36.Contains(IBase36[i].ToString()))
                {
                    v += Array.LastIndexOf(Base36, IBase36[i].ToString()) * bc;
                }
                else
                {
                    throw new InvalidCastException();
                }
            }
            return v;
        }

        /// <summary>
        /// Convert a double into Base36 string
        /// </summary>
        /// <param name="IBase36"></param>
        /// <returns></returns>
        public static string ToBase36(double IBase36)
        {
            string[] Base36 =
                {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                    "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
                };
            string v = null;
            decimal i = default(decimal);
            while (!(IBase36 < 1))
            {
                i = (decimal)(IBase36 % 36);
                v = String.Format(Base36[(int)i] + "{0}", v);
                long result;
                IBase36 = Math.DivRem(Int64.Parse(IBase36.ToString()), (long)36, out result);
            }
            return v;

        }

        public static IEnumerable<string> GetArr(string arr)
        {
            var list = new List<string>();
            foreach (var item in arr)
            {
                list.Add(item.ToString(CultureInfo.InvariantCulture));
            }

            string[] result = list.ToArray();
            return result;
        }

        public static byte[] ConvertFromKey(IEnumerable<string> key)
        {
            List<byte> data = new List<byte>();
            foreach (var b in key)
            {
                if (b.StartsWith("0"))
                {
                    byte datum = (byte)FromBase36(b.Remove(0, 1));
                    data.Add(datum);
                }
                else
                {
                    byte datum = (byte)FromBase36(b);
                    data.Add(datum);
                }

            }
            return data.ToArray();
        }

        /// <summary>
        /// Unscramble an array of bytes
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] UnScrambleData(byte[] data)
        {
            if (data.Length != 10)
                throw new IndexOutOfRangeException("The length of the byte array must be equal to 10");

            var bytes = new List<byte>();
            foreach (var b in data)
            {
                bytes.Add(b);
            }

            byte[] temp = bytes.ToArray();
            temp[0] = data[8];
            temp[1] = data[2];
            temp[2] = data[6];
            temp[3] = data[7];
            temp[4] = data[5];
            temp[5] = data[9];
            temp[6] = data[4];
            temp[7] = data[0];
            temp[8] = data[1];
            temp[9] = data[3];



            return temp;
        }


        /// <summary>
        /// Shifts the bits in an array of bytes to the left
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static bool ShiftLeft(ref byte[] bytes)
        {
            bool leftMostCarryFlag = false;

            // Iterate through the elements of the array from left to right.
            for (int index = 0; index < bytes.Length; index++)
            {
                // If the leftmost bit of the current byte is 1 then we have a carry.
                bool carryFlag = (bytes[index] & 0x80) > 0;

                if (index > 0)
                {
                    if (carryFlag == true)
                    {
                        // Apply the carry to the rightmost bit of the current bytes neighbor to the left.
                        bytes[index - 1] = (byte)(bytes[index - 1] | 0x01);
                    }
                }
                else
                {
                    leftMostCarryFlag = carryFlag;
                }

                bytes[index] = (byte)(bytes[index] << 1);
            }

            return leftMostCarryFlag;
        }

        /// <summary>
        /// Shifts the bits in an array of bytes to the right.
        /// </summary>
        /// <param name="bytes">The byte array to shift.</param>
        public static bool ShiftRight(ref byte[] bytes)
        {
            bool rightMostCarryFlag = false;
            int rightEnd = bytes.Length - 1;

            // Iterate through the elements of the array right to left.
            for (int index = rightEnd; index >= 0; index--)
            {
                // If the rightmost bit of the current byte is 1 then we have a carry.
                bool carryFlag = (bytes[index] & 0x01) > 0;

                if (index < rightEnd)
                {
                    if (carryFlag == true)
                    {
                        // Apply the carry to the leftmost bit of the current bytes neighbor to the right.
                        bytes[index + 1] = (byte)(bytes[index + 1] | 0x80);
                    }
                }
                else
                {
                    rightMostCarryFlag = carryFlag;
                }

                bytes[index] = (byte)(bytes[index] >> 1);
            }

            return rightMostCarryFlag;
        }
    }
}
