﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace Delta.NovoKey
{
    public class LicenseServerWatcher: IDisposable
    {
        static CancellationTokenSource tokenSource = new CancellationTokenSource();
        CancellationToken token = tokenSource.Token;

        public delegate void LicenseServerStatusChangedHandler(object sender, LicenseServerWatcherEventArgs e);
        public event LicenseServerStatusChangedHandler LicenseServerStatusChanged;

        protected virtual void OnLicenseServerStatusChanged(LicenseServerWatcherEventArgs e)
        {
            LicenseServerStatusChangedHandler handler = LicenseServerStatusChanged;
            if (handler != null) handler(this, e);
        }

        public LicenseServerWatcher()
        {
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(NetworkAvailabilityChangedCallback);

            var watchTask = new Task(WatchLicenseServer, token);
            watchTask.Start();
        }

        private void WatchLicenseServer()
        {
            var args = new LicenseServerWatcherEventArgs();
            while (true)
            {
                if(token.IsCancellationRequested)
                    break;
                if (PingLicenseServer.Ping())
                {
                    args.IsAvailable = true;
                    OnLicenseServerStatusChanged(args);
                }
                else
                {
                    args.IsAvailable = false;
                    OnLicenseServerStatusChanged(args);
                }

                Thread.Sleep(5000);
            }
        }

        private void NetworkAvailabilityChangedCallback(object sender, EventArgs e)
        {
            var args = new LicenseServerWatcherEventArgs();
            if (PingLicenseServer.Ping())
            {
                args.IsAvailable = true;
                OnLicenseServerStatusChanged(args);
            }
            else
            {
                args.IsAvailable = false;
                OnLicenseServerStatusChanged(args);
            }
        }


        public void Dispose()
        {
            tokenSource.Cancel();
        }
    }

    public class LicenseServerWatcherEventArgs : EventArgs
    {
        public bool IsAvailable { set; get; }
    }
}
