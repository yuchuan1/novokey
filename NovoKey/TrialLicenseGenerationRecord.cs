﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Delta.NovoKey
{
    [ProtoContract]
    public class TrialLicenseGenerationRecord
    {
        [ProtoMember(1)]
        public bool CanGenerateTrialLicense { get; set; }

        [ProtoMember(2)]
        public List<TrialKeyRecord> TrialProductKeysUsed { get; set; }
    }

    [ProtoContract]
    public class TrialKeyRecord
    {
        [ProtoMember(3)]
        public string keyMD5 { get; set; }

        [ProtoMember(4)]
        public DateTime ExpirationDate { get; set; }
    }
}
