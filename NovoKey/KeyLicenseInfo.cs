﻿namespace Delta.NovoKey
{
    /// <summary>
    /// KeyLicenseInfo
    /// </summary>
    public class KeyLicenseInfo
    {
        /// <summary>
        /// Gets or sets the product ID.
        /// </summary>
        /// <value>
        /// The product ID.
        /// </value>
        public short ProductID { get; set; }

        /// <summary>
        /// Gets or sets the type of the license.
        /// </summary>
        /// <value>
        /// The type of the license.
        /// </value>
        public License.LicenseType LicenseType { get; set; }

        /// <summary>
        /// Gets or sets the num of copies.
        /// </summary>
        /// <value>
        /// The num of copies.
        /// </value>
        public short NumOfCopies { get; set; }
        
        /// <summary>
        /// Gets or sets the trial days.
        /// </summary>
        /// <value>
        /// The trial days.
        /// </value>
        public short TrialDays { get; set; }

        /// <summary>
        /// Gets or sets the product key.
        /// </summary>
        /// <value>
        /// The product key.
        /// </value>
        public string ProductKey { get; set; }
        public int ProductKeyVersion { get; set; }

        /// <summary>
        /// Get Checksum.
        /// </summary>
        /// <returns></returns>
        public virtual int CheckSum()
        {
            return NumOfCopies * 11 + (int)LicenseType * 7 + ProductID * 5 + TrialDays * 3;
        }

        public override string ToString()
        {
            var result = "Product ID: " + ProductID.ToString() + "\n" +
                            "License Type: " + LicenseType.ToString() + "\n" +
                            "Trial Days: " + TrialDays.ToString() + "\n" +
                            "Num Of Copies: " + NumOfCopies.ToString();

            return result;
        }
    }
}
