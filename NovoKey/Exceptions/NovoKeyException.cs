﻿using System;
using System.Runtime.Serialization;

namespace Delta.NovoKey.Exceptions
{
    public class NovoKeyException : Exception
    {
        public NovoKeyException()
        {
        }

        public NovoKeyException(string message)
            : base(message)
        {
        }
    }

    [Serializable]
    public class LicenseNotFoundException : Exception
    {
        public LicenseNotFoundException() { }
        public LicenseNotFoundException(string message) : base(message) { }
        public LicenseNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected LicenseNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class InvalidLicenseException : Exception
    {
        public InvalidLicenseException() { }
        public InvalidLicenseException(string message) : base(message) { }
        public InvalidLicenseException(string message, Exception inner) : base(message, inner) { }
        protected InvalidLicenseException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class TrialKeyExpiredException : Exception
    {
        public TrialKeyExpiredException() { }
        public TrialKeyExpiredException(string message) : base(message) { }
        public TrialKeyExpiredException(string message, Exception inner) : base(message, inner) { }
        protected TrialKeyExpiredException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class InvalidProductKeyException : Exception
    {
        public InvalidProductKeyException() { }
        public InvalidProductKeyException(string message) : base(message) { }
        public InvalidProductKeyException(string message, Exception inner) : base(message, inner) { }
        protected InvalidProductKeyException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class KeyRevokedException : Exception
    {
        public KeyRevokedException() { }
        public KeyRevokedException(string message) : base(message) { }
        public KeyRevokedException(string message, Exception inner) : base(message, inner) { }
        protected KeyRevokedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
    
    [Serializable]
    public class InvalidMachineFingerPrintException : Exception
    {
        public InvalidMachineFingerPrintException() { }
        public InvalidMachineFingerPrintException(string message) : base(message) { }
        public InvalidMachineFingerPrintException(string message, Exception inner) : base(message, inner) { }
        protected InvalidMachineFingerPrintException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class ServerUnreachableException : Exception
    {
        public ServerUnreachableException() { }
        public ServerUnreachableException(string message) : base(message) { }
        public ServerUnreachableException(string message, Exception inner) : base(message, inner) { }
        protected ServerUnreachableException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class LicenseServerErrorException : Exception
    {
        public LicenseServerErrorException() { }
        public LicenseServerErrorException(string message) : base(message) { }
        public LicenseServerErrorException(string message, Exception inner) : base(message, inner) { }
        protected LicenseServerErrorException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class SubscriptionExpiredException : Exception
    {
        public SubscriptionExpiredException() { }
        public SubscriptionExpiredException(string message) : base(message) { }
        public SubscriptionExpiredException(string message, Exception inner) : base(message, inner) { }
        protected SubscriptionExpiredException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class NoMoreActivationException : Exception
    {
        public NoMoreActivationException() { }
        public NoMoreActivationException(string message) : base(message) { }
        public NoMoreActivationException(string message, Exception inner) : base(message, inner) { }
        protected NoMoreActivationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
