﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delta.NovoKey.Exceptions
{
    public class UnableToSaveKeyException : NovoKeyException
    {
        public UnableToSaveKeyException()
        {
        }

        public UnableToSaveKeyException(string message)
            : base(message)
        {
            
        }
    }
}
