﻿namespace Delta.NovoKey.Exceptions
{
    public class InvalidCheckSumException :NovoKeyException
    {
        public InvalidCheckSumException()
        {
        }

        public InvalidCheckSumException(string message)
            : base(message)
        {
            
        }
    }
}
