﻿namespace Delta.NovoKey.DemoApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnFingerPrint = new System.Windows.Forms.Button();
            this.chkProxy = new System.Windows.Forms.CheckBox();
            this.btnValidateLicense = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReadFromLicenseFile = new System.Windows.Forms.Button();
            this.btnGenerateLicenseFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtProxyPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProxyUser = new System.Windows.Forms.TextBox();
            this.txtProxyDomain = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProxyPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProxyAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(575, 404);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.btnClear);
            this.tabPage1.Controls.Add(this.btnFingerPrint);
            this.tabPage1.Controls.Add(this.chkProxy);
            this.tabPage1.Controls.Add(this.btnValidateLicense);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.btnReadFromLicenseFile);
            this.tabPage1.Controls.Add(this.btnGenerateLicenseFile);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtProductKey);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(567, 378);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "License Generator";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(405, 63);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(57, 23);
            this.btnClear.TabIndex = 22;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFingerPrint
            // 
            this.btnFingerPrint.Location = new System.Drawing.Point(289, 63);
            this.btnFingerPrint.Name = "btnFingerPrint";
            this.btnFingerPrint.Size = new System.Drawing.Size(110, 23);
            this.btnFingerPrint.TabIndex = 21;
            this.btnFingerPrint.Text = "Get Finger Print";
            this.btnFingerPrint.UseVisualStyleBackColor = true;
            this.btnFingerPrint.Click += new System.EventHandler(this.btnFingerPrint_Click);
            // 
            // chkProxy
            // 
            this.chkProxy.AutoSize = true;
            this.chkProxy.Location = new System.Drawing.Point(302, 37);
            this.chkProxy.Name = "chkProxy";
            this.chkProxy.Size = new System.Drawing.Size(131, 17);
            this.chkProxy.TabIndex = 20;
            this.chkProxy.Text = "use app proxy settings";
            this.chkProxy.UseVisualStyleBackColor = true;
            // 
            // btnValidateLicense
            // 
            this.btnValidateLicense.Enabled = false;
            this.btnValidateLicense.Location = new System.Drawing.Point(6, 349);
            this.btnValidateLicense.Name = "btnValidateLicense";
            this.btnValidateLicense.Size = new System.Drawing.Size(242, 23);
            this.btnValidateLicense.TabIndex = 19;
            this.btnValidateLicense.Text = "License.ValidateLicense (With license server)";
            this.btnValidateLicense.UseVisualStyleBackColor = true;
            this.btnValidateLicense.Visible = false;
            this.btnValidateLicense.Click += new System.EventHandler(this.ValidateLicense_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 18;
            // 
            // btnReadFromLicenseFile
            // 
            this.btnReadFromLicenseFile.Location = new System.Drawing.Point(151, 63);
            this.btnReadFromLicenseFile.Name = "btnReadFromLicenseFile";
            this.btnReadFromLicenseFile.Size = new System.Drawing.Size(132, 23);
            this.btnReadFromLicenseFile.TabIndex = 17;
            this.btnReadFromLicenseFile.Text = "Read From License File";
            this.btnReadFromLicenseFile.UseVisualStyleBackColor = true;
            this.btnReadFromLicenseFile.Click += new System.EventHandler(this.Read_From_License_File_Click);
            // 
            // btnGenerateLicenseFile
            // 
            this.btnGenerateLicenseFile.Location = new System.Drawing.Point(19, 63);
            this.btnGenerateLicenseFile.Name = "btnGenerateLicenseFile";
            this.btnGenerateLicenseFile.Size = new System.Drawing.Size(126, 23);
            this.btnGenerateLicenseFile.TabIndex = 16;
            this.btnGenerateLicenseFile.Text = "Generate License File";
            this.btnGenerateLicenseFile.UseVisualStyleBackColor = true;
            this.btnGenerateLicenseFile.Click += new System.EventHandler(this.Generate_License_File_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "label2";
            // 
            // txtProductKey
            // 
            this.txtProductKey.Location = new System.Drawing.Point(19, 37);
            this.txtProductKey.Name = "txtProductKey";
            this.txtProductKey.Size = new System.Drawing.Size(264, 20);
            this.txtProductKey.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "label1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtProxyPassword);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtProxyUser);
            this.tabPage2.Controls.Add(this.txtProxyDomain);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtProxyPort);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.txtProxyAddress);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(567, 378);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Proxy Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtProxyPassword
            // 
            this.txtProxyPassword.Location = new System.Drawing.Point(285, 84);
            this.txtProxyPassword.Name = "txtProxyPassword";
            this.txtProxyPassword.PasswordChar = '*';
            this.txtProxyPassword.Size = new System.Drawing.Size(100, 20);
            this.txtProxyPassword.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(222, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Password:";
            // 
            // txtProxyUser
            // 
            this.txtProxyUser.Location = new System.Drawing.Point(96, 85);
            this.txtProxyUser.Name = "txtProxyUser";
            this.txtProxyUser.Size = new System.Drawing.Size(100, 20);
            this.txtProxyUser.TabIndex = 7;
            this.txtProxyUser.Text = "eddie.chen";
            // 
            // txtProxyDomain
            // 
            this.txtProxyDomain.Location = new System.Drawing.Point(79, 55);
            this.txtProxyDomain.Name = "txtProxyDomain";
            this.txtProxyDomain.Size = new System.Drawing.Size(132, 20);
            this.txtProxyDomain.TabIndex = 6;
            this.txtProxyDomain.Text = "delta";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Domain:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "User Name:";
            // 
            // txtProxyPort
            // 
            this.txtProxyPort.Location = new System.Drawing.Point(284, 28);
            this.txtProxyPort.Name = "txtProxyPort";
            this.txtProxyPort.Size = new System.Drawing.Size(45, 20);
            this.txtProxyPort.TabIndex = 3;
            this.txtProxyPort.Text = "8080";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(249, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Port:";
            // 
            // txtProxyAddress
            // 
            this.txtProxyAddress.Location = new System.Drawing.Point(110, 28);
            this.txtProxyAddress.Name = "txtProxyAddress";
            this.txtProxyAddress.Size = new System.Drawing.Size(129, 20);
            this.txtProxyAddress.TabIndex = 1;
            this.txtProxyAddress.Text = "172.16.1.60";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Proxy Address:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(468, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 487);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Key Validator";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnValidateLicense;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReadFromLicenseFile;
        private System.Windows.Forms.Button btnGenerateLicenseFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtProxyPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProxyUser;
        private System.Windows.Forms.TextBox txtProxyDomain;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtProxyPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProxyAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkProxy;
        private System.Windows.Forms.Button btnFingerPrint;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button button1;

    }
}

