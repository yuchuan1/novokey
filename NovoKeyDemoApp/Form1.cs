﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using Delta.NovoKey.Exceptions;


namespace Delta.NovoKey.DemoApp
{
    public partial class Form1 : Form
    {
       // readonly LicenseServerWatcher licenseServerWatcher = new LicenseServerWatcher();
        private int ProductID;
        public Form1()
        {
            ProductID = 1;
            InitializeComponent();
            this.Text += " - " + BackEnd.Platform.ToString() + " " + BackEnd.Version;

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            label1.Text = string.Empty;
            label2.Text = string.Empty;

            //Subscribe to license server status changed event

            //try
            //{
            //    var license = License.ReadLicense(ProductID);
            //    if (license.Type != License.LicenseType.Invalid)
            //        txtProductKey.Text = license.ProductKey;
            //}
            //catch (TrialKeyExpiredException ex)
            //{
            //    label3.Text = ex.ToString();
            //}
            //catch (Exception)
            //{

            //    throw;
            //}


            //if (!license.Activated)
            //    licenseServerWatcher.LicenseServerStatusChanged += LicenseServerStatusChanged;

        }

        private void OnProcessExit(object sender, EventArgs e)
        {
         //   licenseServerWatcher.Dispose();
        }

        private void LicenseServerStatusChanged(object sender, LicenseServerWatcherEventArgs e)
        {
            // if the license server is reachable, try to activate the license.
            if (e.IsAvailable)
            {
                if (License.Activate(ProductID))
                {
                   // licenseServerWatcher.LicenseServerStatusChanged -= LicenseServerStatusChanged;
                   // licenseServerWatcher.Dispose();
                }
            }

        }

        



        private void Generate_License_File_Click(object sender, EventArgs e)
        {
            label2.Text = string.Empty;
            label3.Text = string.Empty;
            string key = txtProductKey.Text.Trim();
            try
            {
                // Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw");
                // Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw", System.Net.WebRequest.GetSystemWebProxy());
                if (chkProxy.Checked)
                    Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw", txtProxyAddress.Text,
                                                 int.Parse(txtProxyPort.Text), txtProxyUser.Text, txtProxyPassword.Text,
                                                 txtProxyDomain.Text);
                else
                {
                    Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw");
                }
                label1.Text = "License generated!";

            }
            catch (UnableToSaveKeyException ex)
            {
                label3.Text = ex.Message;
            }
            catch (InvalidCheckSumException ex)
            {
                label3.Text = ex.Message;
            }
            catch (NoMoreActivationException ex)
            {
                label3.Text = ex.Message;
            }
            catch (InvalidProductKeyException ex)
            {
                label3.Text = ex.Message;
            }
            catch (TrialKeyExpiredException ex)
            {
                label3.Text = ex.Message;
            }
            catch (AccessViolationException ex)
            {
                label3.Text = ex.Message;
            }
            catch (ServerUnreachableException ex)
            {
                label3.Text = ex.Message;
            }
            catch (LicenseServerErrorException ex)
            {
                label3.Text = ex.Message;
            }
            catch (Exception ex)
            {
                label3.Text = ex.ToString();
            }
            

        }

        private void Read_From_License_File_Click(object sender, EventArgs e)
        { 
            KeyLicenseInfo info = License.GetLicenseInfoFromKey(txtProductKey.Text);
            License lic = new License();
            
                if (chkProxy.Checked)
                    lic = License.ReadLicense(info.ProductID, txtProxyAddress.Text, int.Parse(txtProxyPort.Text),
                                              txtProxyUser.Text, txtProxyPassword.Text, txtProxyDomain.Text);
                else
                {
                    try
                    {
                        lic = License.ReadLicense(info.ProductID);
                    }
                    catch (TrialKeyExpiredException ex)
                    {
                        label3.Text = ex.Message;
                    }
                    catch (Exception ex)
                    {
                        label3.Text = ex.Message;
                    }
                }
            
           
            var sb = new StringBuilder();
            sb.Append("Product ID: ");
            sb.AppendLine(info.ProductID.ToString());
            sb.Append("License Type: ");
            switch (lic.Type)
            {
                case License.LicenseType.Subsctription:
                    sb.AppendLine("Subscription");
                    sb.AppendLine(" Expiration Date: " + lic.GetSubscriptionExpirationDate().ToShortDateString());
                    
                    break;
                case License.LicenseType.Free:
                    sb.AppendLine("Free");
                    break;
                case License.LicenseType.Single:
                    sb.AppendLine("Single");
                    break;
                case License.LicenseType.Trial:
                    sb.AppendLine("Trial");
                    break;
                case License.LicenseType.Volume:
                    sb.AppendLine("Volume");
                    break;
                case License.LicenseType.Invalid:
                    sb.AppendLine("Invalid key");
                    break;
            }

            switch (lic.LicenseStatus)
            {
                case License.Status.Invalid:
                    sb.AppendLine("Invalid License key!");
                    break;
                case License.Status.TrialExpired:
                    sb.Append("This ia a trial version. You have ");
                    sb.Append(lic.GetTrialDaysLeft().Days);
                    sb.AppendLine(" days left");
                    sb.AppendLine("Trial period is over, your license is expired!");
                    break;
                case License.Status.Blacklisted:
                    sb.AppendLine("The key you are using is blacklisted! Please get a new key and re-activate your software!");
                    break;
                case License.Status.IsValid:
                    sb.AppendLine("The license key is valid.");
                    sb.AppendLine("Machine fingerprint: " + lic.MachineFingerPrint);
                    sb.AppendLine("Concurrent User Count: " + lic.NumberOfConcurrentUsers);
                    sb.AppendLine("Total Account: " + lic.NumberOfAccounts);

                    break;
                case License.Status.NotFound:
                    sb.AppendLine("License File Not Found!");
                    break;
                case License.Status.IsValidTrialVersion:
                    sb.Append("This ia a trial version. You have ");
                    sb.Append(lic.GetTrialDaysLeft().Days);
                    sb.AppendLine(" days left");

                    break;
            }

            try
            {
               // sb.AppendLine("Remaining installation count: " + License.AvailableInstalations(info.ProductID));
                int remainningCount = 0;
                if (chkProxy.Checked)
                    remainningCount = License.AvailableInstalations(info.ProductID, txtProxyAddress.Text,
                                                                    int.Parse(txtProxyPort.Text), txtProxyUser.Text,
                                                                    txtProxyPassword.Text, txtProxyDomain.Text);
                else
                {
                    remainningCount = License.AvailableInstalations(info.ProductID);
                }

                sb.AppendLine("Remaining installation count: " + remainningCount.ToString());
            }
            catch (Exception ex)
            {
                sb.AppendLine("Remaining installation count: (Unable to contact server - " + ex.Message);
            }
            
            sb.AppendLine("License activation: " + lic.Activated.ToString());

 
            label2.Text = sb.ToString();

        }

        
        private void ValidateLicense_Click(object sender, EventArgs e)
        {
            try
            {
                if (License.ValidateLicense(1, "Eddie Chen", "test"))
                    label2.Text = "License is valid";
            }
            catch (KeyRevokedException ex)
            {
                MessageBox.Show(ex.Message, "Invalid license", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (InvalidProductKeyException ex)
            {
                MessageBox.Show(ex.Message, "Invalid license", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (InvalidMachineFingerPrintException ex)
            {
                MessageBox.Show(ex.Message, "Invalid license", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (LicenseNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Invalid license", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (TrialKeyExpiredException ex)
            {
                MessageBox.Show(ex.Message, "Invalid license", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void btnFingerPrint_Click(object sender, EventArgs e)
        {
            btnFingerPrint.Enabled = false;
            label2.Text += "\nMachine Finger Print: " + FingerPrint.Value();
            btnFingerPrint.Enabled = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            label2.Text = string.Empty;
            label3.Text = string.Empty;
            string key = txtProductKey.Text.Trim();
            try
            {
                // Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw");
                // Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw", System.Net.WebRequest.GetSystemWebProxy());
          
                    Validator.GenerateNewLicense(key, "Eddie Chen", "eddie.chen@delta.com.tw", false, null);
           
                label1.Text = "License generated!";

            }
            catch (UnableToSaveKeyException ex)
            {
                label3.Text = ex.Message;
            }
            catch (InvalidCheckSumException ex)
            {
                label3.Text = ex.Message;
            }
            catch (NoMoreActivationException ex)
            {
                label3.Text = ex.Message;
            }
            catch (InvalidProductKeyException ex)
            {
                label3.Text = ex.Message;
            }
            catch (TrialKeyExpiredException ex)
            {
                label3.Text = ex.Message;
            }
            catch (AccessViolationException ex)
            {
                label3.Text = ex.Message;
            }
            catch (ServerUnreachableException ex)
            {
                label3.Text = ex.Message;
            }
            catch (LicenseServerErrorException ex)
            {
                label3.Text = ex.Message;
            }
            catch (Exception ex)
            {
                label3.Text = ex.ToString();
            }
        }

       
    }
}
