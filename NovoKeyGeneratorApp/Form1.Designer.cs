﻿namespace Delta.NovoKey.GeneratorApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnGenerateMultipleKeys = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGenerateKey = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTrailDays = new System.Windows.Forms.TextBox();
            this.txtNumOfCopies = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbLicenseType = new System.Windows.Forms.ComboBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSeedVersion = new System.Windows.Forms.ToolStripButton();
            this.lblKeyVersion = new System.Windows.Forms.Label();
            this.cmbKeyVersion = new System.Windows.Forms.ComboBox();
            this.txtNumberOfKeys = new System.Windows.Forms.TextBox();
            this.chkSendToServer = new System.Windows.Forms.CheckBox();
            this.comboBoxProduct = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccounts = new System.Windows.Forms.TextBox();
            this.txtConcurrentUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAllowedMachineCount = new System.Windows.Forms.TextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.groupSubscription = new System.Windows.Forms.GroupBox();
            this.dateTimeSubscriptionExpDate = new System.Windows.Forms.DateTimePicker();
            this.groupTrial = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxVendor = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.txtSubscriptionValidDays = new System.Windows.Forms.TextBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.toolStrip1.SuspendLayout();
            this.groupSubscription.SuspendLayout();
            this.groupTrial.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(211, 324);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(302, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Generate Random Key";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(16, 184);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(302, 160);
            this.listBox1.TabIndex = 4;
            this.listBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseUp);
            // 
            // btnGenerateMultipleKeys
            // 
            this.btnGenerateMultipleKeys.Location = new System.Drawing.Point(481, 242);
            this.btnGenerateMultipleKeys.Name = "btnGenerateMultipleKeys";
            this.btnGenerateMultipleKeys.Size = new System.Drawing.Size(152, 23);
            this.btnGenerateMultipleKeys.TabIndex = 5;
            this.btnGenerateMultipleKeys.Text = "Generate Multiple Keys";
            this.btnGenerateMultipleKeys.UseVisualStyleBackColor = true;
            this.btnGenerateMultipleKeys.Click += new System.EventHandler(this.btnGenerateMultipleKeys_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "LicenseType";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Product:";
            // 
            // btnGenerateKey
            // 
            this.btnGenerateKey.Location = new System.Drawing.Point(518, 321);
            this.btnGenerateKey.Name = "btnGenerateKey";
            this.btnGenerateKey.Size = new System.Drawing.Size(132, 23);
            this.btnGenerateKey.TabIndex = 9;
            this.btnGenerateKey.Text = "Generate single key";
            this.btnGenerateKey.UseVisualStyleBackColor = true;
            this.btnGenerateKey.Visible = false;
            this.btnGenerateKey.Click += new System.EventHandler(this.btnGenerateKey_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Valid Days (0 = unlimited)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(458, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Copies";
            // 
            // txtTrailDays
            // 
            this.txtTrailDays.Location = new System.Drawing.Point(137, 22);
            this.txtTrailDays.Name = "txtTrailDays";
            this.txtTrailDays.Size = new System.Drawing.Size(47, 20);
            this.txtTrailDays.TabIndex = 13;
            this.txtTrailDays.Text = "30";
            // 
            // txtNumOfCopies
            // 
            this.txtNumOfCopies.Enabled = false;
            this.txtNumOfCopies.Location = new System.Drawing.Point(503, 135);
            this.txtNumOfCopies.Name = "txtNumOfCopies";
            this.txtNumOfCopies.Size = new System.Drawing.Size(19, 20);
            this.txtNumOfCopies.TabIndex = 14;
            this.txtNumOfCopies.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(328, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Keys to generate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Right click to copy";
            // 
            // cmbLicenseType
            // 
            this.cmbLicenseType.FormattingEnabled = true;
            this.cmbLicenseType.Location = new System.Drawing.Point(86, 41);
            this.cmbLicenseType.Name = "cmbLicenseType";
            this.cmbLicenseType.Size = new System.Drawing.Size(121, 21);
            this.cmbLicenseType.TabIndex = 18;
            this.cmbLicenseType.SelectedIndexChanged += new System.EventHandler(this.cmbLicenseType_SelectedIndexChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSeedVersion});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(672, 25);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnSeedVersion
            // 
            this.btnSeedVersion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSeedVersion.Image = ((System.Drawing.Image)(resources.GetObject("btnSeedVersion.Image")));
            this.btnSeedVersion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSeedVersion.Name = "btnSeedVersion";
            this.btnSeedVersion.Size = new System.Drawing.Size(23, 22);
            this.btnSeedVersion.Text = "SeedVersion";
            this.btnSeedVersion.Click += new System.EventHandler(this.btnSeedVersion_Click);
            // 
            // lblKeyVersion
            // 
            this.lblKeyVersion.AutoSize = true;
            this.lblKeyVersion.Location = new System.Drawing.Point(528, 138);
            this.lblKeyVersion.Name = "lblKeyVersion";
            this.lblKeyVersion.Size = new System.Drawing.Size(73, 13);
            this.lblKeyVersion.TabIndex = 20;
            this.lblKeyVersion.Text = "Seed Version:";
            // 
            // cmbKeyVersion
            // 
            this.cmbKeyVersion.FormattingEnabled = true;
            this.cmbKeyVersion.Location = new System.Drawing.Point(600, 135);
            this.cmbKeyVersion.Name = "cmbKeyVersion";
            this.cmbKeyVersion.Size = new System.Drawing.Size(32, 21);
            this.cmbKeyVersion.TabIndex = 21;
            // 
            // txtNumberOfKeys
            // 
            this.txtNumberOfKeys.Location = new System.Drawing.Point(421, 244);
            this.txtNumberOfKeys.Name = "txtNumberOfKeys";
            this.txtNumberOfKeys.Size = new System.Drawing.Size(45, 20);
            this.txtNumberOfKeys.TabIndex = 22;
            // 
            // chkSendToServer
            // 
            this.chkSendToServer.AutoSize = true;
            this.chkSendToServer.Checked = true;
            this.chkSendToServer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSendToServer.Location = new System.Drawing.Point(490, 107);
            this.chkSendToServer.Name = "chkSendToServer";
            this.chkSendToServer.Size = new System.Drawing.Size(95, 17);
            this.chkSendToServer.TabIndex = 23;
            this.chkSendToServer.Text = "Send to server";
            this.chkSendToServer.UseVisualStyleBackColor = true;
            this.chkSendToServer.Visible = false;
            // 
            // comboBoxProduct
            // 
            this.comboBoxProduct.FormattingEnabled = true;
            this.comboBoxProduct.Location = new System.Drawing.Point(271, 38);
            this.comboBoxProduct.Name = "comboBoxProduct";
            this.comboBoxProduct.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProduct.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(192, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Accounts";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(303, 138);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Concurrent Users";
            // 
            // txtAccounts
            // 
            this.txtAccounts.Location = new System.Drawing.Point(242, 135);
            this.txtAccounts.Name = "txtAccounts";
            this.txtAccounts.Size = new System.Drawing.Size(55, 20);
            this.txtAccounts.TabIndex = 27;
            this.txtAccounts.Text = "1";
            // 
            // txtConcurrentUser
            // 
            this.txtConcurrentUser.Location = new System.Drawing.Point(398, 135);
            this.txtConcurrentUser.Name = "txtConcurrentUser";
            this.txtConcurrentUser.Size = new System.Drawing.Size(54, 20);
            this.txtConcurrentUser.TabIndex = 28;
            this.txtConcurrentUser.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Allowed machine count:";
            // 
            // txtAllowedMachineCount
            // 
            this.txtAllowedMachineCount.Location = new System.Drawing.Point(139, 138);
            this.txtAllowedMachineCount.Name = "txtAllowedMachineCount";
            this.txtAllowedMachineCount.Size = new System.Drawing.Size(47, 20);
            this.txtAllowedMachineCount.TabIndex = 30;
            this.txtAllowedMachineCount.Text = "1";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(331, 200);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(242, 20);
            this.txtPath.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(328, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Path:";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(579, 200);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(71, 23);
            this.btnBrowse.TabIndex = 33;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // groupSubscription
            // 
            this.groupSubscription.Controls.Add(this.radioButton2);
            this.groupSubscription.Controls.Add(this.txtSubscriptionValidDays);
            this.groupSubscription.Controls.Add(this.radioButton1);
            this.groupSubscription.Controls.Add(this.dateTimeSubscriptionExpDate);
            this.groupSubscription.Location = new System.Drawing.Point(12, 68);
            this.groupSubscription.Name = "groupSubscription";
            this.groupSubscription.Size = new System.Drawing.Size(462, 64);
            this.groupSubscription.TabIndex = 34;
            this.groupSubscription.TabStop = false;
            this.groupSubscription.Text = "Subscription License";
            this.groupSubscription.Visible = false;
            // 
            // dateTimeSubscriptionExpDate
            // 
            this.dateTimeSubscriptionExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeSubscriptionExpDate.Location = new System.Drawing.Point(176, 36);
            this.dateTimeSubscriptionExpDate.Name = "dateTimeSubscriptionExpDate";
            this.dateTimeSubscriptionExpDate.Size = new System.Drawing.Size(87, 20);
            this.dateTimeSubscriptionExpDate.TabIndex = 3;
            // 
            // groupTrial
            // 
            this.groupTrial.Controls.Add(this.label4);
            this.groupTrial.Controls.Add(this.txtTrailDays);
            this.groupTrial.Location = new System.Drawing.Point(13, 68);
            this.groupTrial.Name = "groupTrial";
            this.groupTrial.Size = new System.Drawing.Size(454, 56);
            this.groupTrial.TabIndex = 35;
            this.groupTrial.TabStop = false;
            this.groupTrial.Text = "Trial License";
            this.groupTrial.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(408, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Vendor";
            // 
            // comboBoxVendor
            // 
            this.comboBoxVendor.FormattingEnabled = true;
            this.comboBoxVendor.Location = new System.Drawing.Point(456, 41);
            this.comboBoxVendor.Name = "comboBoxVendor";
            this.comboBoxVendor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVendor.TabIndex = 37;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(30, 16);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(176, 17);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Valid days after activation date: ";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // txtSubscriptionValidDays
            // 
            this.txtSubscriptionValidDays.Location = new System.Drawing.Point(204, 15);
            this.txtSubscriptionValidDays.Name = "txtSubscriptionValidDays";
            this.txtSubscriptionValidDays.Size = new System.Drawing.Size(44, 20);
            this.txtSubscriptionValidDays.TabIndex = 5;
            this.txtSubscriptionValidDays.Text = "0";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(30, 39);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(140, 17);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "License Expiration Date:";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 366);
            this.Controls.Add(this.comboBoxVendor);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.groupSubscription);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.txtAllowedMachineCount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtConcurrentUser);
            this.Controls.Add(this.txtAccounts);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxProduct);
            this.Controls.Add(this.chkSendToServer);
            this.Controls.Add(this.txtNumberOfKeys);
            this.Controls.Add(this.cmbKeyVersion);
            this.Controls.Add(this.lblKeyVersion);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.cmbLicenseType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNumOfCopies);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnGenerateKey);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGenerateMultipleKeys);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupTrial);
            this.Name = "Form1";
            this.Text = "NovoKey Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupSubscription.ResumeLayout(false);
            this.groupSubscription.PerformLayout();
            this.groupTrial.ResumeLayout(false);
            this.groupTrial.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnGenerateMultipleKeys;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGenerateKey;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTrailDays;
        private System.Windows.Forms.TextBox txtNumOfCopies;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbLicenseType;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnSeedVersion;
        private System.Windows.Forms.Label lblKeyVersion;
        private System.Windows.Forms.ComboBox cmbKeyVersion;
        private System.Windows.Forms.TextBox txtNumberOfKeys;
        private System.Windows.Forms.CheckBox chkSendToServer;
        private System.Windows.Forms.ComboBox comboBoxProduct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAccounts;
        private System.Windows.Forms.TextBox txtConcurrentUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAllowedMachineCount;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.GroupBox groupSubscription;
        private System.Windows.Forms.DateTimePicker dateTimeSubscriptionExpDate;
        private System.Windows.Forms.GroupBox groupTrial;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxVendor;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox txtSubscriptionValidDays;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}

