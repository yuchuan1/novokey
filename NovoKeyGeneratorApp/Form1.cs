﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Delta.NovoKey.Exceptions;
using Delta.NovoKey.Generator;
using Delta.NovoKey.LicenseServerClient;
using System.Runtime.InteropServices;
using Delta.NovoKey.LicenseServerClient.Models;
using Delta.NovoKey.Models;

namespace Delta.NovoKey.GeneratorApp
{
    public partial class Form1 : Form
    {
        IProductKeyService keyService = new ProductKeyService();
        private ILicenseServerClient _client = new LicenseClient();
        
        [DllImport("wininet.dll", CharSet = CharSet.Auto)]
        static extern bool InternetGetConnectedState(ref InternetConnectionState lpdwFlags, int dwReserved);

        enum InternetConnectionState : int
        {
            INTERNET_CONNECTION_MODEM = 0x1,
            INTERNET_CONNECTION_LAN = 0x2,
            INTERNET_CONNECTION_PROXY = 0x4,
            INTERNET_RAS_INSTALLED = 0x10,
            INTERNET_CONNECTION_OFFLINE = 0x20,
            INTERNET_CONNECTION_CONFIGURED = 0x40
        }

        private static InternetConnectionState flags = 0;
        private bool isConnected = InternetGetConnectedState(ref flags, 0);
        bool isConfigured = (flags & InternetConnectionState.INTERNET_CONNECTION_CONFIGURED) != 0;
        bool isOffline = (flags & InternetConnectionState.INTERNET_CONNECTION_OFFLINE) != 0;
        bool isConnectedUsingModem = (flags & InternetConnectionState.INTERNET_CONNECTION_MODEM) != 0;
        bool isConnectedUsingLAN = (flags & InternetConnectionState.INTERNET_CONNECTION_LAN) != 0;
        bool isProxyUsed = (flags & InternetConnectionState.INTERNET_CONNECTION_PROXY) != 0;
        bool isRasEnabled = (flags & InternetConnectionState.INTERNET_RAS_INSTALLED) != 0;

        public Form1()
        {
            string licenseServerIP = ConfigurationManager.AppSettings["LicenseServerIP"];
            InitializeComponent();

            var settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;

            using (XmlReader reader = XmlReader.Create("vendors.xml", settings))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Text:
                            comboBoxVendor.Items.Add(new ComboboxItem(reader.Value, reader.Value));
                            break;
                    }
                }

                if (comboBoxVendor.Items.Count > 0)
                    comboBoxVendor.SelectedIndex = 0;
            }
            
            txtPath.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            

            // this application can only be run when connected to a LAN
            // and is able to reach the license server
            if (isOffline)
            {
                DialogResult dlgResult = MessageBox.Show(
                    "Internet connection is not available! Please connect to Internet before running this application!",
                    "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                
                if(dlgResult == DialogResult.OK)
                    Environment.Exit(1);
            }
            else
            {
                //var ping = new Ping();
                //var pingReply = ping.Send(licenseServerIP, 1000);
                //if (pingReply.Status != IPStatus.Success)
                try
                {
                    if (!_client.Ping())
                    {
                        DialogResult dlgResult = MessageBox.Show(
                       "Unable to ping license server @" + licenseServerIP,
                       "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        if (dlgResult == DialogResult.OK)
                            Environment.Exit(1);
                    }
                }
                catch (WebException exception)
                {
                    DialogResult dlgResult = MessageBox.Show(
                       "Unable to ping license server @" + licenseServerIP + "\n" + exception.Message,
                       "Error: ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    if (dlgResult == DialogResult.OK)
                        Environment.Exit(1);
                }
                
            }

            try
            {
                int latestVersion = _client.GetLatestSeedVersion().Version;
                for (int i = 1; i <= latestVersion; i++)
                {
                    cmbKeyVersion.Items.Add(new ComboboxItem(i.ToString(), i));
                }

                if (cmbKeyVersion.Items.Count > 0)
                {
                    cmbKeyVersion.SelectedItem = cmbKeyVersion.Items[0];
                }
            }
            catch (WebException ex)
            {
                DialogResult dlgResult = MessageBox.Show(
                     "License Server Error: " + ex.Message,
                     "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                if (dlgResult == DialogResult.OK)
                    Environment.Exit(1);
                
            }
            catch (Exception)
            {
                
                throw;
            }

            try
            {
                var products = _client.GetProducts().Products;
                comboBoxProduct.DataSource = products;
                comboBoxProduct.DisplayMember = "ProductName";
                comboBoxProduct.ValueMember = "ProductId";
            }
            catch (Exception ex)
            {
                DialogResult dlgResult = MessageBox.Show(
                     "License Server Error: " + ex.Message,
                     "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                if (dlgResult == DialogResult.OK)
                    Environment.Exit(1);
            }
            
        }

        private void btnGenerateMultipleKeys_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            btnGenerateMultipleKeys.Text = "Please wait...";
            btnGenerateMultipleKeys.Enabled = false;

            var licenseInfo = new KeyLicenseInfo();
            licenseInfo.ProductID = Int16.Parse(comboBoxProduct.SelectedValue.ToString());
            licenseInfo.LicenseType = (License.LicenseType)Int16.Parse(cmbLicenseType.SelectedValue.ToString());
            licenseInfo.TrialDays = Int16.Parse(txtTrailDays.Text.Trim());
            licenseInfo.NumOfCopies = Int16.Parse(txtNumOfCopies.Text.Trim());

            int max = int.Parse(txtNumberOfKeys.Text.Trim());

            var metadata = new LicenseMetaData();
            metadata.TotalAccounts = Convert.ToInt32(txtAccounts.Text);
            metadata.AllowedMachines = Convert.ToInt32(txtAllowedMachineCount.Text);
            metadata.NumberOfConcurrentUsers = Convert.ToInt32(txtConcurrentUser.Text);

            //metadata.RemainInstallations = Convert.ToInt32(txtAllowedMachineCount.Text);

            if (radioButton1.Checked)
            {
                metadata.SubscriptionValidDays = int.Parse(txtSubscriptionValidDays.Text);
                metadata.SubscriptionExpDate = new DateTime(1980,1,1);
            }
            else if (radioButton2.Checked)
            {
                metadata.SubscriptionValidDays = 0;
                metadata.SubscriptionExpDate = dateTimeSubscriptionExpDate.Value;
            }

            /*
            var keygenTask = Task.Factory.StartNew(() => GenKeys(max, licenseInfo, metadata, Int16.Parse(txtAccounts.Text)), new CancellationToken(), TaskCreationOptions.LongRunning, TaskScheduler.Default);
            keygenTask.ContinueWith(task => MessageBox.Show( max.ToString() + " keys created!"), TaskContinuationOptions.OnlyOnRanToCompletion);
            // keygenTask.Start();
             * */


            var keygenTask = Task.Factory.StartNew(() => GenKeys(max, licenseInfo, metadata, Int16.Parse(txtAccounts.Text)), new CancellationToken(), TaskCreationOptions.LongRunning, TaskScheduler.Default);
            keygenTask.ContinueWith(task => completed(), TaskContinuationOptions.OnlyOnRanToCompletion);
           
        }

        delegate void SetTextCallback(string text);
        public void completed()
        {
            string productName = string.Empty;
            string vendor = string.Empty;
            string licenseType = string.Empty;

            btnGenerateMultipleKeys.Invoke(new MethodInvoker(delegate
            { btnGenerateMultipleKeys.Text = "Generate Multiple Keys"; btnGenerateMultipleKeys.Enabled = true;}));

            comboBoxProduct.Invoke(new MethodInvoker(delegate
            { 
                productName = ((ProductInfo)(comboBoxProduct.SelectedItem)).ProductName;
                vendor = comboBoxVendor.SelectedItem.ToString();
                licenseType = cmbLicenseType.SelectedText;
            }));
            save(productName, vendor, licenseType);
        }

        private void GenKeys(int count, KeyLicenseInfo licenseInfo, LicenseMetaData metadata, int allowedAccounts)
        {
            for (int i = 0; i < count; i++)
            {
                 listBox1.Invoke(new MethodInvoker(delegate
                 { listBox1.Items.Add(keyService.GenerateProductKey(int.Parse(((Delta.NovoKey.GeneratorApp.ComboboxItem)(cmbKeyVersion.SelectedItem)).Value.ToString()), licenseInfo, metadata, true)); }));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ArrayList items = new ArrayList();
            items.Add(new ComboItem("Free", 0));
            items.Add(new ComboItem("Trial", 1));
            items.Add(new ComboItem("Volume", 2));
            items.Add(new ComboItem("Single", 3));
            items.Add(new ComboItem("Subscription", 4));

            cmbLicenseType.DataSource = items;
            cmbLicenseType.DisplayMember = "DisplayText";
            cmbLicenseType.ValueMember = "value";
            
            cmbLicenseType.SelectedIndex = 0;
            txtNumberOfKeys.Text = "10";
        }

        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            listBox1.SelectedIndex = listBox1.IndexFromPoint(e.X, e.Y);
            StringBuilder sb = new StringBuilder();
            
            if (e.Button == MouseButtons.Right)
            {
                foreach (var item in listBox1.Items)
                {
                    sb.AppendLine(item.ToString());
                }
                
              //  Clipboard.SetText(listBox1.SelectedItem.ToString());
                Clipboard.SetText(sb.ToString());
            }
        }

        public class ComboItem
        {
            private string _displayText;
            private int _value;

            public ComboItem(string text, int value)
            {
                this._displayText = text;
                this._value = value;
            }

            public string DisplayText
            {
                get { return _displayText; }
            }

            public int Value
            {
                get { return _value; }
            }
        }

        private void cmbLicenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var type = ((License.LicenseType) (((Form1.ComboItem) (cmbLicenseType.SelectedItem)).Value));
            ResetUI();

            switch (type)
            {
                case License.LicenseType.Trial:
                    groupTrial.Visible = true;
                    txtTrailDays.Text = "30";
                    txtTrailDays.Enabled = true;
                    break;
                case License.LicenseType.Subsctription:
                    groupSubscription.Visible = true;
                    setRadioButtons();
                    break;
                default:
                    break;
            }

        }

        private void ResetUI()
        {
            txtTrailDays.Text = "0";
            txtTrailDays.Enabled = false;
            groupTrial.Visible = false;
            groupSubscription.Visible = false;
            cmbKeyVersion.Enabled = false;
            btnSeedVersion.Enabled = false;
        }

        private void btnSeedVersion_Click(object sender, EventArgs e)
        {
            var t = new Thread(SeedVersionThread);
            t.Start();
        }

        public static void SeedVersionThread()
        {
            Application.Run(new SeedManager());
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            var licenseInfo = new KeyLicenseInfo();

            licenseInfo.ProductID = Int16.Parse(comboBoxProduct.SelectedValue.ToString());
            licenseInfo.LicenseType = (License.LicenseType)Int16.Parse(cmbLicenseType.SelectedValue.ToString());
            licenseInfo.TrialDays = Int16.Parse(txtTrailDays.Text.Trim());
            licenseInfo.NumOfCopies = Int16.Parse(txtNumOfCopies.Text.Trim());

            var metadata= new LicenseMetaData();
            metadata.NumberOfConcurrentUsers = Convert.ToInt32(txtConcurrentUser.Text);
            metadata.AllowedMachines = Convert.ToInt32(txtAllowedMachineCount.Text);
            metadata.TotalAccounts = Convert.ToInt32(txtAccounts.Text);
            

            try
            {
                if(chkSendToServer.Checked)
                    textBox1.Text = keyService.GenerateProductKey((int) ((ComboboxItem) cmbKeyVersion.SelectedItem).Value,
                                                                  licenseInfo, metadata);
                else
                {
                    textBox1.Text = keyService.GenerateProductKey((int)((ComboboxItem)cmbKeyVersion.SelectedItem).Value,
                                                                  licenseInfo, metadata, false);
                }
            }
            catch (UnableToSaveKeyException ex)
            {
                MessageBox.Show("Unable to save key on license server: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtPath.Text = folderDlg.SelectedPath;
            }
        }
     

        private void save(string productName, string vendor, string licenseType)
        {
            var sb = new StringBuilder();
            foreach (var item in listBox1.Items)
            {
                sb.AppendLine(item.ToString());
            }

            string fileName = txtPath.Text + "\\" + vendor + "_" + productName + "_" + licenseType + "_key_list_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +
                              DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() +
                              DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".txt";
            File.WriteAllText(fileName, sb.ToString());

            Process.Start("notepad.exe", fileName);

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            setRadioButtons();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            setRadioButtons();
        }

        private void setRadioButtons()
        {
            if (radioButton1.Checked)
            {
                dateTimeSubscriptionExpDate.Enabled = false;
                txtSubscriptionValidDays.Enabled = true;
            }
            else if (radioButton2.Checked)
            {
                txtSubscriptionValidDays.Text = 0.ToString();
                txtSubscriptionValidDays.Enabled = false;
                dateTimeSubscriptionExpDate.Enabled = true;
            }
        }

    }
}
