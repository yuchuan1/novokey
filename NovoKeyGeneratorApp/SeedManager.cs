﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Delta.NovoKey.Generator;
using Delta.NovoKey.LicenseServerClient;
using Delta.NovoKey.LicenseServerClient.Models;

namespace Delta.NovoKey.GeneratorApp
{
    public partial class SeedManager : Form
    {
        private ILicenseServerClient _client = new LicenseClient();
        public SeedManager()
        {
            InitializeComponent();
            lblLatestVersion.Text = _client.GetLatestSeedVersion().Version.ToString();
        }

        private void btnAddSeeds_Click(object sender, EventArgs e)
        {
            int _version = _client.GetLatestSeedVersion().Version + 1;
            if (_client.AddSeedVersion(_version))
            {
                lblLatestVersion.Text = _client.GetLatestSeedVersion().Version.ToString();
                var seeds = new List<Seed>
                    {
                        new Seed( 1, textBox1.Text),
                        new Seed( 2, textBox2.Text),
                        new Seed( 3, textBox3.Text),
                        new Seed( 4, textBox4.Text),
                        new Seed( 5, textBox5.Text),
                        new Seed( 6, textBox6.Text),
                        new Seed( 7, textBox7.Text),
                        new Seed( 8, textBox8.Text),
                        new Seed( 9, textBox9.Text),
                        new Seed( 10, textBox10.Text),
                        new Seed( 11, textBox11.Text),
                        new Seed( 12, textBox12.Text),
                        new Seed( 13, textBox13.Text),
                        new Seed( 14, textBox14.Text),
                        new Seed( 15, textBox15.Text),
                        new Seed( 16, textBox16.Text),
                        new Seed( 17, textBox17.Text),
                        new Seed( 18, textBox18.Text),
                        new Seed( 19, textBox19.Text),
                        new Seed( 20, textBox20.Text),
                        new Seed( 21, textBox21.Text),
                        new Seed( 22, textBox22.Text),
                        new Seed( 23, textBox23.Text),
                        new Seed( 24, textBox24.Text),
                        new Seed( 25, textBox25.Text)
                    };

                _client.AddSeeds(_version,seeds);
            }

           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
            textBox5.Text = string.Empty;

            textBox6.Text = string.Empty;
            textBox7.Text = string.Empty;
            textBox8.Text = string.Empty;
            textBox9.Text = string.Empty;
            textBox10.Text = string.Empty;

            textBox11.Text = string.Empty;
            textBox12.Text = string.Empty;
            textBox13.Text = string.Empty;
            textBox14.Text = string.Empty;
            textBox15.Text = string.Empty;

            textBox16.Text = string.Empty;
            textBox17.Text = string.Empty;
            textBox18.Text = string.Empty;
            textBox19.Text = string.Empty;
            textBox20.Text = string.Empty;

            textBox21.Text = string.Empty;
            textBox22.Text = string.Empty;
            textBox23.Text = string.Empty;
            textBox24.Text = string.Empty;
            textBox25.Text = string.Empty;

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            var seedVersion = _client.GetLatestSeedVersion();
            var textBoxes = new List<TextBox>
                {
                    textBox1,
                    textBox2,
                    textBox3,
                    textBox4,
                    textBox5,
                    textBox6,
                    textBox7,
                    textBox8,
                    textBox9,
                    textBox10,
                    textBox11,
                    textBox12,
                    textBox13,
                    textBox14,
                    textBox15,
                    textBox16,
                    textBox17,
                    textBox18,
                    textBox19,
                    textBox20,
                    textBox21,
                    textBox22,
                    textBox23,
                    textBox24,
                    textBox25
                };

            var labels = new List<Label>
                {
                    label1,
                    label2,
                    label3,
                    label4,
                    label5,
                    label6,
                    label7,
                    label8,
                    label9,
                    label10,
                    label11,
                    label12,
                    label13,
                    label14,
                    label15,
                    label16,
                    label17,
                    label18,
                    label19,
                    label20,
                    label21,
                    label22,
                    label23,
                    label24,
                    label25
                };

            for (var i = 0; i < seedVersion.SeedCount; i++)
            {
                textBoxes[i].Text = seedVersion.Seeds[i].SeedValue;
                labels[i].Text = seedVersion.Seeds[i].SeedID.ToString();
            }

        }

        
    }
}
