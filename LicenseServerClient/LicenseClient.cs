﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Delta.NovoKey.LicenseServerClient.Models;
using Delta.NovoKey.Models;
using RestSharp;

namespace Delta.NovoKey.LicenseServerClient
{
    public class LicenseClient : ILicenseServerClient
    {
        public bool AddKey(KeyLicenseInfo licenseInfo, LicenseMetaData metadata)
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["key"]
            };

            var request = new RestRequest { Method = Method.PUT };
            request.AddParameter("keyValue", licenseInfo.ProductKey);
            request.AddParameter("productID", licenseInfo.ProductID);
            request.AddParameter("licenseType",(int)licenseInfo.LicenseType);
            request.AddParameter("trialDays", licenseInfo.TrialDays);
            request.AddParameter("numberOfCopies", licenseInfo.NumOfCopies);

            request.AddParameter("concurrentUsers", metadata.NumberOfConcurrentUsers);
            request.AddParameter("allowedMachines", metadata.AllowedMachines);
            request.AddParameter("allowedAccounts", metadata.TotalAccounts);

            request.AddParameter("subscriptionValidDays", metadata.SubscriptionValidDays);
            request.AddParameter("subscriptionExpDate", metadata.SubscriptionExpDate.ToShortDateString());
            

            var response = (RestResponse)client.Execute(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (response.Content == "200")
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            
            return false;
        }

        public bool AddKeys(List<KeyLicenseInfo> licenseInfos, LicenseMetaData metadata)
        {
            var result = true;
            foreach (var info in licenseInfos)
            {
                if (!AddKey(info, metadata))
                    result = false;
            }

            return result;
        }
        
        public bool AddSeed(int version, Seed seed)
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["seed"]
            };

            var request = new RestRequest { Method = Method.PUT };
            request.AddParameter("seedID", seed.SeedID);
            request.AddParameter("seedValue", seed.SeedValue);
            request.AddParameter("version", version);

            var response = (RestResponse)client.Execute(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (response.Content == "200")
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateSeed(Seed seed)
        {
            throw new NotImplementedException();
        }

        public bool DeleteSeed(Seed seed)
        {
            throw new NotImplementedException();
        }

        public bool AddSeeds(int version, List<Seed> seeds)
        {
            var result = true;
            foreach (var seed in seeds)
            {
                if (!AddSeed(version, seed))
                    result = false;
            }

            return result;
        }

        public SeedVersion GetLatestSeedVersion()
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["LatestSeedVersion"]
            };

            var request = new RestRequest { Method = Method.GET };
            var response = (RestResponse)client.Execute(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return SeedVersion.Parse(response.Content);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public SeedVersion GetSeedVersion(int versionNumber)
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["SeedVersion"]
            };

            var request = new RestRequest { Method = Method.GET };
            request.AddParameter("version", versionNumber);
            var response = (RestResponse)client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return SeedVersion.Parse(response.Content);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        public bool AddSeedVersion(int versionNumber)
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["SeedVersion"]
            };

            var request = new RestRequest { Method = Method.PUT };
            request.AddParameter("version", versionNumber);
            var response = (RestResponse)client.Execute(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (response.Content == "200")
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public bool UpdateSeedVersion(int versionNumber)
        {
            throw new NotImplementedException();
        }

        public bool DeleteSeedVersion(int versionNumber)
        {
            throw new NotImplementedException();
        }

        public bool Ping()
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["ping"]
            };

            client.BaseUrl = "https://service.novosync.com/ping";

            var request = new RestRequest { Method = Method.GET };

            var response = (RestResponse)client.Execute(request);

                if (response.ResponseStatus != ResponseStatus.Completed)
                    throw response.ErrorException;
                    
                if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        if (response.Content == "1")
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
           
        }

        public ProductInfoCollection GetProducts()
        {
            IRestClient client = new RestClient
            {
                BaseUrl = ConfigurationManager.AppSettings["BaseURL"]
                          + ConfigurationManager.AppSettings["ProductInfo"]
            };

            var request = new RestRequest { Method = Method.GET };
            var response = (RestResponse)client.Execute(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WebException(response.ErrorMessage);

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return ProductInfoCollection.Parse(response.Content);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
    }
}
