﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Delta.NovoKey.LicenseServerClient.Models;
using Delta.NovoKey.Models;

namespace Delta.NovoKey.LicenseServerClient
{
    public interface ILicenseServerClient
    {
        bool AddKey(KeyLicenseInfo licenseInfo, LicenseMetaData metadata);
        bool AddKeys(List<KeyLicenseInfo> licenseInfos, LicenseMetaData metadata);

        bool AddSeed(int version, Seed seed);
        bool UpdateSeed(Seed seed);
        bool DeleteSeed(Seed seed);

        bool AddSeeds(int version, List<Seed> seeds);

        SeedVersion GetLatestSeedVersion();
        SeedVersion GetSeedVersion(int seedVersion);
        bool AddSeedVersion(int versionNumber);
        bool UpdateSeedVersion(int versionNumber);
        bool DeleteSeedVersion(int versionNumber);

        bool Ping();
        ProductInfoCollection GetProducts();
        //bool AddSeedVersion();
    }
}
