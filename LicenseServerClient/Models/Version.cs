﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Delta.NovoKey.LicenseServerClient.Models
{
    public class SeedVersion
    {
        public int Version { get; set; }
        public bool Released { get; set; }
        public int SeedCount { get; set; }
        public List<Seed> Seeds { get; set; }

        public static SeedVersion Parse(string json)
        {
            try
            {
                var seedVersion = JsonConvert.DeserializeObject<SeedVersion>(json);
                return seedVersion;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}
