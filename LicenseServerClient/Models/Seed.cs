﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delta.NovoKey.LicenseServerClient.Models
{
    public class Seed
    {
        public Seed(int seedID, string seedValue)
        {
            SeedID = seedID;
            SeedValue = seedValue;
            
        }
        public int SeedID { get; set; }
        public string SeedValue { get; set; }

        
    }
}
