﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Delta.NovoKey.LicenseServerClient.Models
{
    public class ProductInfo
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
    }
}
