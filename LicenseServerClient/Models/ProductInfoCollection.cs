﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Delta.NovoKey.LicenseServerClient.Models
{
    public class ProductInfoCollection
    {
        [JsonProperty(PropertyName = "products")]
        public List<ProductInfo> Products { get; set; }

        public static ProductInfoCollection Parse(string json)
        {
            try
            {
                var productInfo = JsonConvert.DeserializeObject<ProductInfoCollection>(json);
                return productInfo;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
