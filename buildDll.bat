copy C:\Jenkins\workspace\NovoKey\NovoKeyDemoApp\bin\Release\NovoKey.dll C:\Jenkins\workspace\NovoKey\JNI\bin
copy C:\Jenkins\workspace\NovoKey\NovoKeyDemoApp\bin\Release\NovoKeyValidator.dll C:\Jenkins\workspace\NovoKey\JNI\bin
copy C:\Jenkins\workspace\NovoKey\NovoKeyDemoApp\bin\Release\protobuf-net.dll C:\Jenkins\workspace\NovoKey\JNI\bin
cd C:\Jenkins\workspace\NovoKey\JNI\bin
set JAVA_HOME="C:\Program Files\Java\jre6"
proxygen.exe NovoKey.dll -wd NovoKey
proxygen.exe NovoKeyValidator.dll -wd NovoKeyValidator