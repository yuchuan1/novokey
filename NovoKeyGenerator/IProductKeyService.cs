﻿using System;
using Delta.NovoKey.LicenseServerClient.Models;
using Delta.NovoKey.Models;

namespace Delta.NovoKey.Generator
{
    public interface IProductKeyService
    {
       // string GenerateProductKey();
        string GenerateProductKey(int version, KeyLicenseInfo licenseInfo, LicenseMetaData metadata);
        string GenerateProductKey(int version, KeyLicenseInfo licenseInfo, LicenseMetaData metadata, bool sendToLicenseServer);
        string GenerateFingerPrint();
    }
}
