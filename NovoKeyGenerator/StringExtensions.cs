﻿using System;

namespace Delta.NovoKey.Generator
{
    public static class StringExtensions
    {
        public static string Reverse(this string input)
        {
            char[] chars = input.ToCharArray();
            Array.Reverse(chars);
            return new String(chars);
        }
    }
}
