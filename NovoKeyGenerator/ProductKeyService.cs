﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Delta.NovoKey.Exceptions;
using Delta.NovoKey.LicenseServerClient;
using Delta.NovoKey.LicenseServerClient.Models;
using Delta.NovoKey.Models;
using NLog;

namespace Delta.NovoKey.Generator
{
    public class ProductKeyService :IProductKeyService
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        // Change this value when changing to a different version of seeds
        private const int SeedVersion = 1;
        private ILicenseServerClient _client = new LicenseClient();

        public string GenerateProductKey(int version, KeyLicenseInfo licenseInfo, LicenseMetaData metadata)
        {
            return GenerateProductKey(version, licenseInfo, metadata, true);
        }

        public string GenerateProductKey(int version, KeyLicenseInfo licenseInfo, LicenseMetaData metadata, bool sendToLicenseServer)
        {
            // Make the variable that holds the serial
            string serial = null;

            // 
            // Make the random key (within base 36)
            //
            long key = NextInt(0, 60466175);

            //
            // Create the list that will contain the 'Char Arrays'
            //
            var lst = new List<List<string>>();
            var original = new List<List<string>>();
            // ################################################################
            // #  IMPORTANT!!!!                                               #
            // #                                                              #
            // #  Initialize the arrays used in generating a key.             #
            // #  THIS IS YOUR MAIN WEAPON AGAINST HACKERS!!!                 #
            // #      MAKE IT DIFFERENT!!!!!                                  #
            // #                                                              #
            // ################################################################

            string arr1 = string.Empty, arr2 = string.Empty, arr3 = string.Empty, arr4 = string.Empty, arr5 = string.Empty, arr6 = string.Empty, arr7 = string.Empty, arr8 = string.Empty, arr9 = string.Empty, arr10 = string.Empty, arr11 = string.Empty, arr12 = string.Empty, arr13 = string.Empty, arr14 = string.Empty, arr15 = string.Empty, arr16 = string.Empty, arr17 = string.Empty, arr18 = string.Empty, arr19 = string.Empty, arr20 = string.Empty, arr21 = string.Empty, arr22 = string.Empty, arr23 = string.Empty, arr24 = string.Empty, arr25 = string.Empty;
            var seeds = new List<List<string>> { CConvert.GetArr(arr1).ToList(), CConvert.GetArr(arr2).ToList(), CConvert.GetArr(arr3).ToList(), CConvert.GetArr(arr4).ToList(), CConvert.GetArr(arr5).ToList(), CConvert.GetArr(arr6).ToList(), CConvert.GetArr(arr7).ToList(), CConvert.GetArr(arr8).ToList(), CConvert.GetArr(arr9).ToList(), CConvert.GetArr(arr10).ToList(), CConvert.GetArr(arr11).ToList(), CConvert.GetArr(arr12).ToList(), CConvert.GetArr(arr13).ToList(), CConvert.GetArr(arr14).ToList(), CConvert.GetArr(arr15).ToList(), CConvert.GetArr(arr16).ToList(), CConvert.GetArr(arr17).ToList(), CConvert.GetArr(arr18).ToList(), CConvert.GetArr(arr19).ToList(), CConvert.GetArr(arr20).ToList(), CConvert.GetArr(arr21).ToList(), CConvert.GetArr(arr22).ToList(), CConvert.GetArr(arr23).ToList(), CConvert.GetArr(arr24).ToList(), CConvert.GetArr(arr25).ToList() };
            
            var seedVersion = _client.GetSeedVersion(version);
            for (var i = 0; i < seedVersion.SeedCount; i++)
            {
                seeds[i] = CConvert.GetArr(seedVersion.Seeds[i].SeedValue).ToList();
                lst.Add(seeds[i]);
            }

            if (lst.Count.Equals(0))
            {
                throw new NovoKeyException("No seeds found, please check your IP is in the authorized group on the server!");
            }

            // Convert the key to Base36 and prepend to the serial code

                serial += CConvert.ToBase36(key);
            // Append extra 0's if the key isn't already five characters long
            while (serial.Length != 5)
            {
                serial = "0" + serial;
            }

            licenseInfo.ProductKeyVersion = 1;

            string[] embeddedData = GenerateEmbeddedData(licenseInfo, serial);

            //
            // Generate the key using the unique 'array' for each character.
            //

            var rng = new RandomNumber();
            while (serial.Length != 35)
            {
                int x = serial.Length;
                // Use modulus to see if this is the time for a hyphen ("-")
                if (x % 6 == 5)
                {
                    serial += "-";
                }
                else if (x + 1 == 8)
                {
                    serial += embeddedData[0];
                }
                else if (x + 1 == 10)
                {
                    serial += embeddedData[1];
                }
                else if (x + 1 == 14)
                {
                    serial += embeddedData[2];
                }
                else if (x + 1 == 16)
                {
                    serial += embeddedData[3];
                }
                else if (x + 1 == 19)
                {
                    serial += embeddedData[4];
                }
                else if (x + 1 == 21)
                {
                    serial += embeddedData[5];
                }
                else if (x + 1 == 25)
                {
                    serial += embeddedData[6];
                }
                else if (x + 1 == 27)
                {
                    serial += embeddedData[7];
                }
                else if (x + 1 == 31)
                {
                    serial += embeddedData[8];
                }
                else if (x + 1 == 33)
                {
                    serial += embeddedData[9];
                }
                else
                {
                    var temp = rng.GetRandomNumber((uint)key, 0, lst[x - (5 + (x + 1) / 6)].Count - 1);
                    serial += lst[x - (5 + (x + 1) / 6)][temp];
                }
            }

            licenseInfo.ProductKey = serial;

            if (sendToLicenseServer)
            if (!_client.AddKey(licenseInfo, metadata))
                throw new UnableToSaveKeyException("Error adding key to the server!");

            // Return the serial key
            return serial;
        }

        private string[] GenerateEmbeddedData(KeyLicenseInfo licenseInfo, string seed)
        {
            var checksum = licenseInfo.CheckSum();

            var proid = BitConverter.GetBytes(licenseInfo.ProductID + 3420);
            var lictype = BitConverter.GetBytes((int)licenseInfo.LicenseType + 1571);
            var trialdays = BitConverter.GetBytes(licenseInfo.TrialDays + 4725);
            var numofcopies = BitConverter.GetBytes(licenseInfo.NumOfCopies + 7458);
           // var keyversion = BitConverter.GetBytes(licenseInfo.ProductKeyVersion + 3531);
            var chk = BitConverter.GetBytes(checksum);

            byte[] hiddenData;
            using (var memStream = new MemoryStream())
            {
                memStream.Write(proid, 0, 2);
                memStream.Write(lictype, 0, 2);
                memStream.Write(trialdays, 0, 2);
                memStream.Write(numofcopies, 0, 2);
                memStream.Write(chk, 0, 2);
                //memStream.Write(keyversion,0,2);
                hiddenData = memStream.ToArray();
            }
            
            CConvert.ShiftLeft(ref hiddenData);
            string[] list = ConvertToKey(hiddenData);

            return list;
        }


        public string GenerateFingerPrint()
        {
            return FingerPrint.Value();
        }

        private static int NextInt(int min, int max)
        {
            var rng = new RNGCryptoServiceProvider();
            var buffer = new byte[4];

            rng.GetBytes(buffer);
            var result = BitConverter.ToInt32(buffer, 0);

            return new Random(result).Next(min, max);
        }
        
       
        private string[] ConvertToKey(IEnumerable<byte> data)
        {
            var result = new List<string>();
            foreach (var b in data)
            {
                string temp = CConvert.ToBase36(Double.Parse(b.ToString(CultureInfo.InvariantCulture)));
                var k = CConvert.FromBase36("T");

                if (temp == null)
                    temp = "00";

                if (temp.Length < 2)
                    temp = "0" + temp;

                result.Add(temp);
            }
            return result.ToArray();
        }
    }
}
